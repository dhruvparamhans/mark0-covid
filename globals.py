import os


h_cols =    ['t',
             'u',
             'bust',
             'Pavg',
             'Wavg',
             'S',
             'Atot',
             'firm-savings',
             'debt-tot',
             'inflation',
             'pi-avg',
             'propensity',
             'k',
             'Dtot',
             'rhom',
             'rho',
             'rhop',
             'pi-used',
             'tau-tar',
             'tau-meas',
             'R', 
            'Wtot',
            'etap-avg',
            'etam-avg', 
            'w-nominal',
            'Ytot',
             'deftot', 
            'profits',
            'debt-ratio',
            'firms-alive',
            'left',
            'u-af', 
            'bustaf']
            

cols = ['R0',
        'theta',
        'alpha_g',
        'rho0',
        'alpha',
        'alpha_pi',
        'alpha_e',
        'Gamma0',
        'pi_star',
        'e_star',
        'seed',
        'gammap',
        'eta0m',
        'tau_meas',
        'tau_tar',
        'wage_factor',
        'y0']

path_plots = 'plots/'

flagnames = ['base','cons_pure','firing_shock', 'cons_prod','cons_theta', 'fire_debt', 'prod_debt']
fnames = [os.path.join('output', flagname) for flagname in flagnames]

# torun = pd.DataFrame(fnames, columns=['fnames'])
# torun['R0'] = 2.0
# torun['theta'] = 3.0

# torun['Gamma0'] = 0.0
# torun['alpha_pi'] = 0.0
# torun['alpha_e'] = 0.0
# torun['pi_star'] = 0.0
# torun['e_star'] = 0.0
# torun['tau_tar'] = 0.0
# torun['wage_factor'] = 1.0
# torun['y0'] = 0.5
# torun['seed'] = 0
# torun['gammap'] = 0.1
# torun['eta0m'] = 0.2


fd = 2900
td = 3200

# torun['rho0'] = 0.0

# torun['alpha_g'] = 0.0
# torun['alpha'] = 0.0
# torun['tau_meas'] = 0.0
