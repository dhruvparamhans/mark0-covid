
shockflags=(0 1 2 3 4 5 6)
fnames=("base_" "cons_pure" "firing_shock" "cons_prod" "cons_theta" "fire_debt" "prod_debt")


t_starts=(500 1000 1500 2000 3000)


shockflag=1
nb_runs=500

# for shockflag in 1 2 3
# do
#     for i in {451..500}
#     do
# 	for t_start in ${t_starts[@]}
# 	do
# 	    output=${fnames[$shockflag]}_${t_start}_${i}
# 	    echo $output
# 	    t_end=$(($t_start+5))
# 	    extra_start=$t_end
# 	    extra_end=$(($extra_start+5))
# 	    policy_start=$t_start
# 	    policy_end=$t_end
# 	    #echo $t_start,$t_end,$extra_start $extra_end
# 	    sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,extra_start=$extra_start,extra_end=$extra_end,policy_start=$policy_start,policy_end=$policy_end slurm_script.sh
# 	done
#     done
# done


# for shockflag in 4 5 6
# do
#     for i in {451..500}
#     do
# 	for t_start in ${t_starts[@]}
# 	do
# 	    output=${fnames[$shockflag]}_${t_start}_${i}
# 	    echo output
# 	    t_end=$(($t_start+5))
# 	    extra_start=$t_end
# 	    extra_end=$(($extra_start+5))
# 	    policy_start=$t_start
# 	    policy_end=$t_end
# 	    #echo $t_start,$t_end,$extra_start $extra_end
# 	    sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,extra_start=$extra_start,extra_end=$extra_end,policy_start=$policy_start,policy_end=$policy_end slurm_script.sh
# 	done
#     done
# done

########################################### Consumption Factor Experiments #############################################################
## Variables for checking influence of c_0
#lengths=(7 8 9 10)
#lengths=(4)
#factors=(0.02 0.05 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.99)
#factors=(0.4 0.5)
#factors=(0.02 0.05 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5)
#factors=(0.61 0.62 0.63 0.64 0.65 0.66 0.67 0.68 0.69)
#factors=(0.4)
## We will only export shockflag, output, t_start and t_end
##
##

# shockflag=4
# t_start=2000
# for length in ${lengths[@]}
# do
# 	for factor in ${factors[@]}
# 	do
# 		for i in {1..100}
# 		do
# 		    output=${fnames[$shockflag]}_${length}_${factor}_${i}
# 		    t_end=$(($t_start+$length))
# 		    policy_start=$t_start
# 		    policy_end=$t_end
# 		    echo $output, $t_end	
# 		    sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,policy_start=$policy_start,policy_end=$policy_end,factor=$factor slurm_script.sh
# 		done
# 	done
# done


########################################### Consumption Factor + Policy Experiments #############################################################

# slengths=(4 5 6 7 8 9 10)
# plengths=(3 4 5 6 7 8 9 10 11 12 13 14)
# factors=(0.1 0.2 0.3)
# shockflag=4
# t_start=2000
# for slength in ${slengths[@]}
# do
# 	for factor in ${factors[@]}
# 	do
# 	    for plength in ${plengths[@]}
# 	    do
# 		for i in {41..50}
# 		do
# 		    output=${fnames[$shockflag]}_${slength}_${plength}_${factor}_${i}
# 		    t_end=$(($t_start+$slength))
# 		    policy_start=$t_start
# 		    policy_end=$(($policy_start+$plength))
# 		    echo $output, $t_end
# 		    sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,policy_start=$policy_start,policy_end=$policy_end,factor=$factor slurm_script.sh
# 		done
# 	    done
# 	done
# done


########################################### Consumption + Prod Shock Experiments #############################################################

# shockflag=3
# lengths=(3)
# factors=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.95)
# zfactors=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.95)
# t_start=2000
# for length in ${lengths[@]}
# do
#     for factor in ${factors[@]}
#     do
# 	for zfactor in ${zfactors[@]}
# 	do
# 	    for i in {501..600}
# 	    do
# 		output=${fnames[$shockflag]}_${length}_${factor}_${zfactor}_${i}
# 		t_end=$(($t_start+$length))
# 		echo $output, $t_end
# 		sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,factor=$factor,zfactor=$zfactor slurm_script.sh
# 	    done
# 	done
#     done
# done




########################################### Pure Consumption Shock - Varying Lengths ##############################################################

shockflag=1
lengths=(3 6 9)
factors=(0.02 0.05 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.95)
zfactor=1.0
t_start=2000
for length in ${lengths[@]}
do
    for factor in ${factors[@]}
    do
	for i in {1..100}
	do
	    output=${fnames[$shockflag]}_${length}_${factor}_${zfactor}_${i}
	    t_end=$(($t_start+$length))
	    echo $output, $t_end
	    sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,factor=$factor,zfactor=$zfactor slurm_script.sh
	done
    done
done



########################################### Consumption + Prod Shock + Policy Experiments ###########################################################

# shockflag=6
# slengths=(4 5 6 7 8 9 10)
# plengths=(3 4 5 6 7 8 9 10 11 12 13 14)
# cfactor=0.7
# zfactor=0.5
# t_start=2000
# helico=0
# adapt=1
# gpolicy=1
# for slength in ${slengths[@]}
# do
#     for plength in ${plengths[@]}
#     do
# 	for i in {1..100}
# 	do
# 	    t_end=$(($t_start+$slength))
# 	    #policy_start=$(($t_start+$slength))
# 	    policy_start=$t_start
# 	    policy_end=$(($policy_start+$plength))
	    
# 	    output=${fnames[$shockflag]}_${slength}_${plength}_${cfactor}_${zfactor}_${i}
# 	    echo $output, $t_end
# 	    sbatch --export=seed=$i,output=$output,shockflag=$shockflag,t_start=$t_start,t_end=$t_end,policy_start=$policy_start,policy_end=$policy_end,factor=$cfactor,zfactor=$zfactor,gpolicy=$gpolicy,adapt=$adapt slurm_script.sh
# 	done
#     done
# done

