for t_start in 1500 2000 2500 3000
do
	python script_statistics.py -shockflag=1 -t_start=$t_start -nb_runs=500
done


for shockflag in {2..6}
do
	for t_start in 500 1000 1500 2000 2500 3000
	do
		python script_statistics.py -shockflag=$shockflag -t_start=$t_start -nb_runs=500
	done
done

