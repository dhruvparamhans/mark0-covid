#!/usr/bin/env bash

#SBATCH --partition Short

#SBATCH -o ./output/slurm-%j.out

#./covid --seed=${seed} --output=${output} --shockflag=${shockflag} --t_start=${t_start} --t_end=${t_end} --policy_start=${policy_start} --policy_end=${policy_end} --factor=${factor} --zfactor=${zfactor}


./covid --seed=${seed} --output=${output} --shockflag=${shockflag} --t_start=${t_start} --t_end=${t_end} --factor=${factor} --zfactor=${zfactor}
