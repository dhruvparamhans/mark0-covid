h_cols =    ['t',
             'u',
             'bust',
             'Pavg',
             'Wavg',
             'S',
             'Atot',
             'firm-savings',
             'debt-tot',
             'inflation',
             'pi-avg',
             'propensity',
             'k',
             'Dtot',
             'rhom',
             'rho',
             'rhop',
             'pi-used',
             'tau-tar',
             'tau-meas',
             'R', 
            'Wtot',
            'etap-avg',
            'etam-avg', 
            'w-nominal',
            'Ytot',
             'deftot', 
            'profits',
            'debt-ratio',
            'firms-alive',
             'left',
             'u-af',
             'bust-af',
             'frag',
             'true_end',
             'theta',
             'p_var', 'w_var']


import os
import glob
import numpy as np
import pandas as pd
from argparse import ArgumentParser 
flagnames = ['cons_pure', 'firing_shock', 'cons_prod', 'cons_theta', 'fire_debt', 'prod_debt']
flag_dict = { x+1: flagnames[x] for x in range(6)}

paths = [flagname + '/' for flagname in flagnames]


def find_intersection(data, threshold):
    #taken from here: https://stackoverflow.com/a/23868444
    
    thresholded_data = data <= threshold
    threshold_edges = np.convolve([1, -1], thresholded_data, mode='same')

    crisis_ends = np.where(threshold_edges == 1)[0]
    crisis_starts = np.where(threshold_edges == -1)[0]
    return crisis_starts,crisis_ends


def get_all_files(shockflag):
    flagname = flag_dict[shockflag]
    path = flagname + '/'
    return [os.path.basename(x) for x in glob.glob(path + flagname + '*')]


def read_file(fname):
    tmp = pd.read_csv(fname, sep="\t", header = None)
    tmp.columns = h_cols
    return tmp

def get_t_start(fname):
    return float(fname.split('_')[-2])

def get_index(fname):
    return float(fname.split('_')[-1].split('.')[0])


def get_length(fname):
    return float(fname.split('_')[-3])
def get_factor(fname):
    return float(fname.split('_')[-2])



def get_error_rate_limited(shock_length, factor, shockflag=1, t_start=1):


    flagname = flag_dict[shockflag]

    all_files = get_all_files(shockflag)

    filter_length = list(filter(lambda x: get_length(x) == shock_length, all_files))

    filter_factor = list(filter(lambda x: get_factor(x) == factor, filter_length))

    error =[]
    for fname in filter_factor:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)

        if crisis_ends.size < 1:
            error.append(1)
        else:
            error.append(0)

    return np.mean(error)

def get_crisis_times_fixed_length(shock_length, shockflag=1, t_start=1000):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)

    filter_files = list(filter(lambda x: get_length(x) == shock_length), all_files)

    index = []
    factors = []
    crisis_times = [] # only a single crisis time


    error = []

    for fname in filter_files:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)

        seed = get_index(fname)
        factor = get_factor(fname)

        index.append(seed)
        factor.append(factor)

        if (crisis_ends.size >1):
            crisis_times.append(crisis_ends[1])
            error.append(0)
        else:
            crisis_times.append(len(res['u'][t_start+1:]))
            error.append(1)
    return pd.DataFrame({
        'seed': index,
        'crisis': crisis_times,
        'factor': factors,
        'length' : length
    })



def generate_stats(shockflag, t_start):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)
    filter_files = list(filter(lambda x: get_t_start(x) == t_start, all_files))
    index = []
    crisis_times = []
    crisis_times_2 = []
    between_crisis_times = []
    total_crisis_times = []
    error = []
    
    for fname in filter_files:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)

        seed = get_index(fname)
        index.append(seed)

        if (shockflag >=4):
            if (crisis_ends.size > 1):
                crisis_times_2.append(crisis_ends[-1] - crisis_start[-1])
                between_crisis_times.append(crisis_start[-1] - crisis_ends[1])
                total_crisis_times.append(crisis_ends[-1])
        if (crisis_ends.size >1):
            crisis_times.append(crisis_ends[1])
            error.append(0)
            #print('seed = {}, t-final = {}'.format(seed, crisis_ends[1]))
        else:
            crisis_times.append(len(res['u'][t_start+1:]))
            error.append(1)
            #print('seed = {}, t-final = error'.format(seed))
    if (shockflag >=4):
        return pd.DataFrame({
            'seed': index, 
            'first-crisis': crisis_times,
            'second-crisis': crisis_times_2,
            'between' : between_crisis_times, 
            'total' : total_crisis_times,
            'is_error': error})
    
#         return crisis_times, crisis_times_2, between_crisis_times, total_crisis_times
    else:
        return pd.DataFrame({
            'seed': index,
            'crisis': crisis_times,
            'error': error})



def compute_stats_and_save(shockflag):
    basename = 'stats'
    t_starts = [500, 1000, 1500, 2000, 3000]

    for t_start in t_starts:
        print("Running for T-Start = {}".format(t_start))
        stats_df = generate_stats(shockflag, t_start)
        fname = basename + '_{}_{}.csv'.format(flag_dict[shockflag], t_start)
        stats_df.to_csv(fname, sep="\t", index = None)


# for shockflag in range(1,7):
#     print("Running for Shock = {}".format(flag_dict[shockflag]))
#     compute_stats_and_save(shockflag)
def get_error_data_single_file(shockflag, fname, t_start, t_end):

    flagname = flag_dict[shockflag]

    res = read_file(os.path.join(flagname, fname))

    crisis_starts, crisis_ends = find_intersection(res['u'][t_start:], 0.1)
    is_double = 0
    second_crisis = 0
    crisis_times = 0
    is_crisis = 0
    error = 0
    max_u_during = np.max(res['u'][t_start:t_end])
    max_u_after = np.max(res['u'][t_end:])
    if (crisis_ends.size > 1):
        ## There is a crisis
        if (crisis_starts.size >1):
            ## Multiple crises
            ## First crisis length = crisis_ends[1] - crisis_starts[0]
            crisis_times = crisis_ends[1] - crisis_starts[0]
            is_crisis = 1
            is_double = 1
            if crisis_ends.size >2:
                ## Crisis is finitely long
                second_crisis = crisis_ends[2] - crisis_starts[1]
                error = 0
            else:
                ## Crisis is infinitely long
                error = 1
                second_crisis = crisis_ends[-1] - crisis_starts[1]
        else:
            ## Single crisis
            crisis_times = crisis_ends[1] - crisis_starts[0]
            is_double = 0
            second_crisis = 0
            is_crisis = 1
            error = 0
    else:
        if (crisis_starts.size < 1):
             ## Then no crisis
            is_crisis = 0
            crisis_times = 0
            error = 0
            is_double = 0
            second_crisis=0
        else:
            crisis_times = len(res['u'][t_start+1:])
            is_crisis = 1
            error = 1
            is_double = 0
            second_crisis = 0
    return crisis_times, is_crisis, error, max_u_during, max_u_after, is_double, second_crisis


def get_shocklength(fname):
    return float(fname.split('_')[2])
def get_policylength(fname):
    return float(fname.split('_')[-3])


def get_stats_policy(shockflag, t_start, is_policy = True):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)

    sim_number = []
    cfactors = []
    zfactors = []
    slengths = []
    plengths = []

    crisis_times_list = []
    max_u_during_list = []
    max_u_after_list = []

    error_list = []
    is_crisis_list = []

    is_double_list = []
    second_crisis_list = []

    for fname in all_files:
        tmp = fname.split('_')
        
        shocklength = float(tmp[2])
        policylength = float(tmp[3])
        cfactor = float(tmp[4])
        zfactor = float(tmp[5])
        index = get_index(fname)
        crisis_times, is_crisis, error, max_u_during, max_u_after, is_double,  second_crisis = get_error_data_single_file(shockflag, fname, t_start, int(t_start+shocklength))

        cfactors.append(cfactor)
        zfactors.append(zfactor)
        sim_number.append(index)
        crisis_times_list.append(crisis_times)
        error_list.append(error)
        is_crisis_list.append(is_crisis)
        max_u_during_list.append(max_u_during)
        max_u_after_list.append(max_u_after)
        slengths.append(shocklength)
        plengths.append(policylength)
        is_double_list.append(is_double)
        second_crisis_list.append(second_crisis)

        print("Index = {}, Length = {}, Factor = {}, T-crisis = {}".format(index, shocklength, cfactor, crisis_times))
        
    return pd.DataFrame({
        'index': sim_number,
        'slength': slengths,
        'plength': plengths,
        'error': error_list,
        'is-crisis': is_crisis_list,
        'cfactor': cfactors,
        'T-crisis': crisis_times_list,
        'maxu': max_u_during_list,
        'maxuaf': max_u_after_list,
        'second-crisis': second_crisis_list,
        'is-double' : is_double_list,
        'zfactor': zfactors})

    

def get_complete_error_rates(t_start, shockflag):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)

    index = []
    factors= []
    lengths = []
    crisis_times = []
    error = []
    is_crisis = []
    for fname in all_files:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)
        lengths.append(get_length(fname))
        index.append(get_index(fname))
        factors.append(get_factor(fname))

        if (crisis_ends.size > 1):
            crisis_times.append(crisis_ends[1] - crisis_start[0])
            error.append(0)
            is_crisis.append(1)
            print("Index = {}, Length = {}, Factor = {}, T-crisis = {}".format(get_index(fname), get_length(fname), get_factor(fname), crisis_ends[1]))
        else:
            if (crisis_start.size < 1):
                ## Then no crisis
                is_crisis.append(0)
                crisis_times.append(0)
                error.append(1)
                print("Index = {}, Length = {}, Factor = {}, No Crisis".format(get_index(fname), get_length(fname), get_factor(fname)))
            else:
                crisis_times.append(len(res['u'][t_start+1:]))
                is_crisis.append(1)
                error.append(1)
                print("Index = {}, Length = {}, Factor = {}, T-crisis = error".format(get_index(fname), get_length(fname), get_factor(fname)))

    return pd.DataFrame({
        'index': index,
        'factor': factors,
        'length' : lengths,
        'error' : error,
        'is-crisis': is_crisis, 
        'T-crisis': crisis_times})


def get_stats_prod(shockflag=3, t_start=2000):

    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)

    sim_number = []
    cfactors = []
    zfactors = []
    slengths = []
    
    crisis_times_list = []
    max_u_during_list = []
    max_u_after_list = []

    error_list = []
    is_crisis_list = []

    is_double_list = []
    second_crisis_list = []

    for fname in all_files:
        tmp = fname.split('_')
        
        shocklength = float(tmp[2])
        cfactor = float(tmp[-3])
        
        zfactor = float(tmp[-2])
        index = get_index(fname)
        crisis_times, is_crisis, error, max_u_during, max_u_after, is_double, second_crisis  = get_error_data_single_file(shockflag, fname, t_start, int(t_start+shocklength))

        cfactors.append(cfactor)
        zfactors.append(zfactor)
        sim_number.append(index)
        crisis_times_list.append(crisis_times)
        error_list.append(error)
        is_crisis_list.append(is_crisis)
        max_u_during_list.append(max_u_during)
        max_u_after_list.append(max_u_after)
        slengths.append(shocklength)
        is_double_list.append(is_double)
        second_crisis_list.append(second_crisis)

        print(" Index = {}, Length = {}, cfactor = {}, zfactor = {} T-crisis = {} Second crisis = {} Error = {}".format(index, shocklength, cfactor, zfactor, crisis_times, second_crisis, error))        
    return pd.DataFrame({
        'index': sim_number,
        'slength': slengths,
        'error': error_list,
        'is-crisis': is_crisis_list,
        'cfactor': cfactors,
        'zfactor': zfactors, 
        'T-crisis': crisis_times_list,
        'second-crisis': second_crisis_list,
        'is-double' : is_double_list,
        'maxu': max_u_during_list,
        'maxuaf': max_u_after_list})

    


parser = ArgumentParser(description = "Compute stats for given shock")
parser.add_argument("-f", "--filename", type = str,help = "Filename to which data is written")
parser.add_argument("-s", "--shockflag", type = int, help = "Shockflag")
parser.add_argument("--t_start", type = int, help = "When shock begins")

args = parser.parse_args()

if __name__ == "__main__":

    print(args)
    shockflag = args.shockflag
    t_start = args.t_start
    fname = args.filename
    print(flag_dict[shockflag])
    complete_df = get_stats_prod(shockflag, t_start)
#    complete_df = get_stats_policy(shockflag, t_start)

    complete_df.to_csv(fname+'.csv', sep = "\t", index = None)
    
