from runner_utils import *
# os.chdir('/Users/PhDStuff/my-phd-projects/code_covid_send/build/')

def run_single_instance(shockflag, param_names, param_values, program = './covid'):
    output = run_program_default(param_names, param_values, program)

    res = read_output([fnames[shockflag]], h_cols)
    return res[flagnames[shockflag]]


def find_intersection(data, threshold):
    #taken from here: https://stackoverflow.com/a/23868444
    
    thresholded_data = data < threshold
    threshold_edges = np.convolve([1, -1], thresholded_data, mode='same')

    crisis_ends = np.where(threshold_edges == 1)[0]
    crisis_starts = np.where(threshold_edges == -1)[0]
    return crisis_starts,crisis_ends


def run_with_different_seed(shockflag, nb_runs = 200, t_start = 500, policy_end = None,etam_new = 0, seed_default = -1):
    if policy_end is not None:
        p_end = policy_end
    else:
        p_end = t_start + 5
    if (shockflag == 3):
        param_names = ['shockflag', 'seed', 't_start', 't_end', 'extra_start', 'extra_end', 'etam_new']
        param_values = [shockflag, 0, t_start, t_start+5, t_start+5, t_start+10, etam_new]
    elif (shockflag ==4 or shockflag == 5):
        param_names = ['shockflag', 'seed', 't_start', 't_end', 'policy_start', 'policy_end', 'etam_new']        
        param_values = [ shockflag,0, t_start, t_start+5, t_start, p_end, etam_new]
    elif (shockflag == 6):
        param_names = ['shockflag', 'seed', 't_start', 't_end', 'extra_start', 'extra_end', 'policy_start', 'policy_end', 'etam_new'] 
        param_values = [ shockflag, 0, t_start, t_start + 5, t_start+5, t_start + 10, t_start, p_end, etam_new]
    else:
        param_names = ['shockflag', 'seed', 't_start', 't_end', 'etam_new']
        param_values = [shockflag, 0, t_start, t_start + 5, etam_new] # We start with zero and then we change
    crisis_times = []
    crisis_times_2 = []
    between_crisis_times = []
    total_crisis_times = []
    error = [] # stores whether there is an error or not 
    
    
#     fname = flagnames[shockflag]
#     fname += '_' + '{}'.format(t_start) + '.csv'
    if (seed_default != -1):
        seed_range = range(nb_runs)
    else:
        seed_range = np.ones(nb_runs)*(seed_default)
    for i in tqdm(range(nb_runs), position=0, leave = True, desc = "progress"):
        param_values[1] = seed_range[i]
        output = run_program_default(param_names, param_values, './covid')
        res = read_output([fnames[shockflag]], h_cols)[flagnames[shockflag]]
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)
        
        if (shockflag >=4):
            if (crisis_ends.size > 1):
                crisis_times_2.append(crisis_ends[-1] - crisis_start[-1])
                between_crisis_times.append(crisis_start[-1] - crisis_ends[1])
                total_crisis_times.append(crisis_ends[-1])
        if (crisis_ends.size >1):
            crisis_times.append(crisis_ends[1])
            error.append(0)
            print(f'seed = {seed_range[i]}, t-final = {crisis_ends[1]}')
        else:
            crisis_times.append(len(res['u'][t_start+1:]))
            error.append(1)
            print(f'seed = {seed_range[i]}, t-final = error,')
    if (shockflag >=4):
        return pd.DataFrame({
            'first-crisis': crisis_times,
            'second-crisis': crisis_times_2,
            'between' : between_crisis_times, 
            'total' : total_crisis_times,
            'is_error': error})
    
#         return crisis_times, crisis_times_2, between_crisis_times, total_crisis_times
    else:
        return pd.DataFrame({
            'crisis': crisis_times,
            'error': error})

def run_with_different_start_time(shockflag, nb_runs=200, t_start = 500, etam_new = 0, seed_default= 0):
    param_names = ['shockflag', 'seed', 't_start', 't_end', ]
    param_values = [shockflag, seed_default, t_start, t_start + 5]
    t_starts = t_start + np.arange(0,nb_runs*10, 10)
    t_ends = t_starts + 5

    crisis_times = []
    crisis_times_2 = []
    between_crisis_times = []
    total_crisis_times = []
    error =[]
    
    
    for i in tqdm(range(nb_runs), position=0, leave = True, desc = "progress"):
        param_values[-2] = t_starts[i]
        param_values[-1] = t_ends[i]
        
        output = run_program_default(param_names, param_values, './covid')
        res = read_output([fnames[shockflag]], h_cols)[flagnames[shockflag]]
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)
        
        if (shockflag >=4):
            if (crisis_ends.size > 1):
                crisis_times_2.append(crisis_ends[-1] - crisis_start[-1])
                between_crisis_times.append(crisis_start[-1] - crisis_ends[1])
                total_crisis_times.append(crisis_ends[-1])
        if (crisis_ends.size >1):
            crisis_times.append(crisis_ends[1])
            error.append(0)
            print(f'T-start = {t_starts[i]}, t-final = {crisis_ends[1]}')
        else:
            crisis_times.append(len(res['u'][t_start+1:]))
            error.append(1)
            print(f'T-start = {t_starts[i]}, t-final = error,')
    if (shockflag >=4):
        return pd.DataFrame({
            't-start': t_starts,
            'first-crisis': crisis_times,
            'second-crisis': crisis_times_2,
            'between' : between_crisis_times, 
            'total' : total_crisis_times,
            'is_error': error})
    else:
        return pd.DataFrame({
            't-start' : t_starts,
            'crisis': crisis_times,
            'error': error})
# class ComputeStats:
    
#     def __init__(self, shockflag, t_start = 500, t_end = 505):
#         self.shockflag = shockflag
#         self.t_start = t_start
#         self.t_end = t_end

#         self.has_extra = False
#         self.has_policy = False


#     def add_extra(self, extra_start, extra_end):
#         self.extra_start = extra_start
#         self.extra_end =  extra_end

#         self.has_extra = True

#     def add_policy(self, policy_start, policy_end):

#         self.policy_start = policy_start
#         self.policy_end = policy_end

#         self.has_policy = False

#     def run_single_instance(param_names, param_values, program='./covid'):

#         output = run_program_default(param_names, param_values, program_name=program)

#         res = read_output([fnames[self.shockflag]], h_cols)

#         return res[flagnames[self.shockflag]]


#     def run_multiple_same_seed(param_names,
