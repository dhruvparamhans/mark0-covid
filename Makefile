CFLAGS          = -O3 -pedantic --std=c++11
LIBS            =  -L/usr/lib -lgsl -lgslcblas -lm
INCL            = -I/usr/include
COMP            = g++
CC		= gcc
%:%.cpp 
	$(COMP) $<  $(CFLAGS) $(INCL) -o $@

%:%.c
	$(CC) $<  $(CFLAGS) $(LIBS) $(INCL) -o $@


.PHONY : clean
clean:
	rm -f *.o covid covid-base
