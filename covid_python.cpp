#include <pybind11/pybind11.h>
#include "main_loop.hpp"


namespace py = pybind11;

PYBIND11_MODULE(covid_python, m){
    m.doc() = R"pbdoc(
      PyBind11 example plugin
      -----------------------
      .. currentmodule:: cmake_example

      .. autosummary::
         :toctree: _generate


      mark0loop
)pbdoc";

    m.def("loop", &mark0loop, R"pbdoc(
         Run mark0loop
)pbdoc");


   //  m.def("add", &add, R"pbdoc(
//        Add two numbers

//        Adds two numbers

// )pbdoc");

//     m.def("subtract", &subtract,R"pbdoc(
//         Subtract two numbers
// )pbdoc");


#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif

}
