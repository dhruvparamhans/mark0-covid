# Macroeconomic Shocks to Mark0 

The code can be built and run from within the `runner.ipynb` notebook. 

For compiling and running the program on the command line, simply do 

```shell
make covid
./covid 
```

The program takes a certain number of parameters as input. Without these inputs, the program runs with a default values:

```bash
./covid [OPTION...]

      --R0          Ratio of hiring-firing rate (default: 2.0)
  -t, --theta       Maximum credit supply available to firms (default: 3.0)
  -g, --Gamma0      Baseline parameter - Influence of fragility on hiring and firing rates (default: 0.0)
  -r, --rho0        Natural interest rate (default: 0.0)
  -a, --alpha       Influence of deposit rates on consumption (default: 0.0)
  -b, --alpha_pi    Intensity of interest rate policy of central bank (default: 0.0)
  -x, --alpha_e     Influence of employment on interest rate policy of central bank (default: 0.0)
  -p, --pi_star      Inflation Target (default: 0.0)
      --e_star       Employment Target (default: 0.0)
      --tau_tar      Inflation target parameter (default: 0.0)
  -w, --wage_factor  Factor to adjust wages to inflation expectations (default: 1.0)
  -y, --y0           Initial production (default: 0.5)
      --gammap       Parameter to set adjustment of prices (default: 0.1)
  -e, --eta0m        Sensitivity of production to excess supply (default: 0.2)
      --tau_meas     Realised inflation parameter (default: 0.0)
      --alpha_g      Influence of loans interest rate on hiring-firing policy (default: 0.0)
  -s, --seed arg     seed for random number generation (default: 0)
  -f, --shockflag arg Flag to set kind of shock (default: 0)
      --t_start arg   Time when shock occurs (default: 3000)
      --t_end arg     Time when shock end (default: 3005)
      --debt_start arg Time when debt shock occurs (default: 3000)
      --debt_end arg   Time when debt shock ends (default: 3005)
  -h, --help            Print usage
```

To pass your own values to these parameters, you can simply do

```shell
./covid --theta=3.5 --alpha_g=50.0 
```

For the shocks, 4 values are accepted:

1. `--shockflag=0`: No shock (default)
2. `--shockflag=1`: Consumption shock 
3. `--shockflag=2`: Debt shock 
4. `--shockflag=3`: Production shock
5. `--shockflag=4`: Consumption and Debt shock. 
