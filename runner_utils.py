import pandas as pd
import os
import matplotlib.pyplot as plt
cmap = plt.get_cmap('Paired')
import sys
import numpy as np
from tqdm import tqdm_notebook as tqdm
plt.style.use('seaborn-ticks')

path_plots = 'plots'
PATH_TO_COVID =os.getcwd()


h_cols =    ['t',
             'u',
             'bust',
             'Pavg',
             'Wavg',
             'S',
             'Atot',
             'firm-savings',
             'debt-tot',
             'inflation',
             'pi-avg',
             'propensity',
             'k',
             'Dtot',
             'rhom',
             'rho',
             'rhop',
             'pi-used',
             'tau-tar',
             'tau-meas',
             'R', 
            'Wtot',
            'etap-avg',
            'etam-avg', 
            'w-nominal',
            'Ytot',
             'deftot', 
            'profits',
            'debt-ratio',
            'firms-alive',
            'left',
            'u-af', 
            'bust-af', 
            'frag', 
            'true_end',
            'theta',
            'p-var', 
            'w-var',
             'ytot-temp', 
             'min-ytot']
            

cols = ['R0',
        'theta',
        'alpha_g',
        'rho0',
        'alpha',
        'alpha_pi',
        'alpha_e',
        'Gamma0',
        'pi_star',
        'e_star',
        'seed',
        'gammap',
        'eta0m',
        'tau_meas',
        'tau_tar',
        'wage_factor',
        'y0']

path_plots = 'plots/'

flagnames = ['base','cons_pure','firing_shock', 'cons_prod','cons_theta', 'fire_debt', 'prod_debt']
fnames = [os.path.join('output', flagname) for flagname in flagnames]
def read_output(fnames, h_cols, res_dir=PATH_TO_COVID):
    res = {}
#     print(res_dir)
    for f in fnames:
#         fn = '%s%s.txt' % (res_dir, f)
        fn = os.path.join(res_dir, f+'.txt')
        tmp = pd.read_csv(fn, sep='\t', header=None)
        assert len(tmp.columns) == len(h_cols)
        tmp.columns= h_cols
        res[f.replace('output/','')] = tmp.copy()
    res = pd.concat(res, 1)
    return res

def generate_param_list(param_names, param_values, base_string='./hello'):
    for param_name, param_value in zip(param_names, param_values):
        base_string += f" --{param_name}={param_value} "
    return base_string

def dump_and_run(torun0, cols, program_name='./covid'):
    torun = torun0[cols].copy()
    torun['run'] = program_name
#     torun['command'] = torun[['run']+cols].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    torun['command'] = torun[cols].apply(lambda row: generate_param_list(cols, row.values.astype(str), program_name), axis=1)
    torun = torun['command']
#     torun = pd.concat([pd.Series('make covid'),torun])
    torun.to_csv(os.path.join(PATH_TO_COVID, 'run'), header=False, index=False)
#     for index in torun.index:
#         output_system = os.system(torun[index])
#     return output_system
    return torun


def run_program_default(param_names, param_values, program_name = './covid'):
    program_string = generate_param_list(param_names, param_values, program_name)
    if (not os.path.isfile('covid')):
        print("Compiling program")
        os.system('make covid')
    program_output = os.system(program_string)
    return program_output

def compute_nominal_wage(df, shock_start=3001, shock_end = 3005):
    p_avg = df['Pavg']
    w_avg = df['Wavg']
    
    w_nominal = df['Wavg'].copy()
    
    for t in range(shock_start+1, shock_end+1):
#         print(t, np.arange(shock_start, t))
        w_nominal[t] = w_nominal[t]*np.prod(p_avg[shock_start:t])/p_avg[shock_start]
    df['w-nominal'] = w_nominal
    return df

def find_intersection(data, threshold):
    #taken from here: https://stackoverflow.com/a/23868444
    
    thresholded_data = data <= threshold
    threshold_edges = np.convolve([1, -1], thresholded_data, mode='same')

    crisis_ends = np.where(threshold_edges == 1)[0]
    crisis_starts = np.where(threshold_edges == -1)[0]
    return crisis_starts,crisis_ends



def type_crisis(res, t_start):
    crisis_starts, crisis_ends = find_intersection(res['u'][t_start:], 0.1)
    if crisis_ends.size > 1:
        ## There is indeed a crisis 
        if crisis_starts.size > 1:
        # multiple crises
        # first crisis length = crisis_ends[1] - crisis_starts[0]
            print("First crisis length = ", crisis_ends[1] - crisis_starts[0])
        # is there a second crisis
            if crisis_ends.size >2:
            # there is a second crisis with finite time 
                print("Second crisis. Start = {} length ={} ".format(crisis_starts[1], crisis_ends[2] - crisis_starts[1]))
            else:
            # Crisis is infinitely long
                print("Second crisis is infinite. Second crisis start = ", crisis_starts[1])
        else:
        # single crisis
            print(f"Single crisis. Length = {crisis_ends[1] - crisis_starts[0]}, Starts = {crisis_starts[0]}")
    else:
        if crisis_starts.size < 1:
            print("No crisis", crisis_ends)
        else:
            print("Infinite crisis. Starts at ", crisis_starts[0])

def run_and_read(param_names, param_values, program_name = './covid'):
    print(generate_param_list(param_names, param_values, program_name))
    out = run_program_default(param_names, param_values, program_name)
    shockflag = param_values[0]
    return read_output([fnames[shockflag]], h_cols)[flagnames[shockflag]]

def plot_single_variable(df, var, fd = 2990, td = 3030, plot_object = plt):
    plot_object.plot(df[var][fd:td])
    
def t_plot(res, fd,td, title = None):
    fig = plt.figure(figsize=(12, 8), dpi=200)
#     top = res[s]
    cc = 1
    titles = ['Unemployment', 'Prices/Wages', 'Bankruptcies', 'Savings', 'Variances', 'Output']
    cols = ['u','Pavg','bust-af',['Atot','S'], ['p-var', 'w-var'], 'Ytot']
    for c in range(len(cols)) :
        ax = fig.add_subplot(3,2,cc)
        cc += 1
        ax = res[cols[c]][fd:td].plot(ax=ax)
#         ax.set_ylabel(c, size=12)
        ax.set_xlabel('')
        ax.set_title(titles[c], size=12)
    if title is None:
        plt.suptitle(s, fontsize=15)
    else:
        plt.suptitle(title, fontsize=15)
    
    plt.tight_layout()
    plt.subplots_adjust(top=0.9)

def load_firm_variables(t = 2990):
    fname = "firms_{}".format(t) + '.txt'
    
    fname = os.path.join('output', fname)
    
    if (not os.path.isfile(fname)):
        print ("File doesn't exist")
        return
    else:
        return np.loadtxt(fname)
    
