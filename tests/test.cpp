#include "../cxxopts.hpp"


int main(int argc, char** argv){

  cxxopts::Options options("test", "Testing command line options");

  options.add_options()
    ("b,bar", "Param bar", cxxopts::value<double>()->default_value("0.5"))
        ("d,debug", "Enable debugging", cxxopts::value<double>()->default_value("1.0"))
        ("f,foo", "Param foo", cxxopts::value<double>()->default_value("10"))
    ("o, output", "Output filename", cxxopts::value<std::string>()->default_value("base"))
        ("h,help", "Print usage")
    ;


  std::vector<std::string> param_names = {"debug", "bar", "foo"};
  double bar, debug, foo;
  std::string output_name; 

  std::vector<double * > ptr_vec = {&debug, &bar, &foo};

  
  // cxxopts::ParseResult result = options.parse(argc, argv);
  // cxxopts::Options options  = parse_cmdline(argc, argv);

  //  auto result = results.result;
  // auto options = results.options;
  auto result = options.parse(argc, argv);
  
    if (result.count("help"))
    {
      std::cout << options.help() << std::endl;
      exit(0);
    }

    for(int i = 0; i< param_names.size(); i++){
      *ptr_vec[i] = result[param_names[i]].as<double>();
    }

    output_name = result["o"].as<std::string>(); 

    std::cout << "Bar" <<  "\t" << "Debug" << "\t" << "Foo" << "\t" << "Output name" << std::endl;
    std::cout << bar << "\t" << debug <<  "\t" << foo << "\t" << output_name << std::endl;
    //    bool debug = result["debug"].as<bool>();
    // std::string bar;
    // if (result.count("bar"))
    //   bar = result["bar"].as<double>();
    // int foo = result["foo"].as<int>();
    // std::cout << "Bar: "<< bar << " foo "<< foo << " Debug count " << result.count("debug") << " Debug value " << result["debug"].as<bool>() << std::endl;
  //   std::vector<double> param_vector;
  //   if (result.count("i")){
  //     param_vector = result["i"].as<std::vector<double>>();
  //   }
    
    auto keyvalues = result.arguments();
    for(auto it = result.arguments().begin(); it != result.arguments().end(); it++){
      std::cout << it->key() << "\t" << it->value() << std::endl;
    }
  //   std::vector<std::string> param_names = get_params(result);
  //   std::vector<double> param_values;
  //   get_param_values(result, param_values);
  //   print_vector(param_names);
  //   print_vector(param_values);
  //   // for (auto it = param_names.begin(); it != param_names.end(); it++){
  //   //   std::cout << *it << std::endl;
  //   // }


  //    std::cout << "Initialization list" << std::endl;
    //    print_vector(param_vector);
    // double param1, param2;

    // std::vector< double * > temp_param_vector_ptr = {&param1, &param2};
    // std::vector<double> temp_param_vector = {10.0, 20.0};
    // // for (auto i = 0; i < temp_param_vector.size(); i++){
    // //   *temp_param_vector_ptr[i] = temp_param_vector[i];
    // // }

    // map_bw_vects(temp_param_vector_ptr, temp_param_vector);
    
    
    // print_vector(temp_param_vector);
    // std::cout << "param1, param2  " << param1 << "\t" << param2<<  std::endl;
    // // auto groups = options.groups();

    // // for (auto it = groups.begin(); it != groups.end(); it++){
    // //   std::cout << *it << std::endl;
    // // }


  // cxxopts::Options options("test", "Testing command line options");
  
  // options.add_options()
  //   ("R0", "Ratio of hiring-firing rate", cxxopts::value<double>()->default_value("2.0"))
  // ("t,theta", "Maximum credit supply available to firms", cxxopts::value<double>()->default_value("3.0"))
  // ("g,Gamma0", "Gamma0", cxxopts::value<double>()->default_value("0.0"))
  // ("r,rho0", "Rho0", cxxopts::value<double>()->default_value("0.0"))
  // ("a,alpha", "alpha", cxxopts::value<double>()->default_value("0.0"))
  // ("b,alpha_pi", "alpha_pi", cxxopts::value<double>()->default_value("0.0"))
  // ("x,alpha_e", "alpha_e", cxxopts::value<double>()->default_value("0.0"))
  // ("p,pi_star", "pi_star", cxxopts::value<double>()->default_value("0.0"))
  // ("e_star", "e_star", cxxopts::value<double>()->default_value("0.0"))
  // ("tau_tar", "tau_tar", cxxopts::value<double>()->default_value("0.0"))
  // ("w,wage_factor", "wage factor", cxxopts::value<double>()->default_value("1.0"))
  // ("y,y0", "y0", cxxopts::value<double>()->default_value("0.5"))
  // ("gammap", "gammap", cxxopts::value<double>()->default_value("0.1"))
  // ("e,eta0m", "eta0m", cxxopts::value<double>()->default_value("0.2"))
  // ("tau_meas", "tau_meas", cxxopts::value<double>()->default_value("0.0"))
  // ("alpha_g", "alpha_g", cxxopts::value<double>()->default_value("0.0"))
  // ("s,seed", "seed", cxxopts::value<double>()->default_value("0.0"))
  // ("f,foutput", "Filename where output is stored", cxxopts::value<std::string>()->default_value("base"))
  // ("h,help", "Print usage");



  // auto result = options.parse(argc, argv);

  // if (result.count("help")){
  //   std::cout << options.help() << std::endl;
  // }

  // if(result.count("R0") == 1){
  //   std::cout << result["R0"].as<double>() << std::endl;
  // }

  // std::cout << "Sanity check " << result.count("R0") << std::endl;
  
    

  // auto keyvalues = result.arguments();

  // for (auto it = keyvalues.begin(); it != keyvalues.end(); it++){
  //   std::cout << it->key() << "\t" << it->value() << std::endl;
  // }
  

  

  
    return 0;
}
