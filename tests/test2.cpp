
//#include "../utils.hpp"
//
// #include "../json.hpp"
// #include "../utils.hpp"
#include <iostream>
#include <random>
#include <fstream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sf.h>
#include <time.h>
// using json = nlohmann::json;

const gsl_rng_type *gslT ;
gsl_rng *gslr ;

int main(int argc, char** argv){

  int seed = 0;

  if (seed == -1){
    seed = time( NULL );
  }

  std::mt19937 gen(seed);
  std::uniform_real_distribution<double> dis(0,1);

  for (int n = 0; n < 10; n++){
    std::cout << dis(gen) << " ";
  }
  std::cout << std::endl;

  seed = 4; 
  std::mt19937 gen2(seed);
  std::uniform_real_distribution<double> dis2(0,1);

  for (int n = 0; n < 10; n++){
    std::cout << dis2(gen2) << " ";
  }
  std::cout << std::endl;


  gsl_rng_env_setup() ;
  gslT = gsl_rng_default ;
  gslr = gsl_rng_alloc(gslT) ;
  gsl_rng_set(gslr,seed);

  std::cout << "GSL random number generation" << std::endl;

  for (int n= 0; n < 10; n++){
    std::cout << gsl_rng_uniform(gslr) << " ";
  }

  std::cout << std::endl; 

  // for (int n = 0; n < 10; n++){
  //   std::cout << dis(gen) << " ";
  // }
  // std::cout << std::endl;
  // return 0; 

  // std::vector<double> output_vector;

  // output_vector = std::vector<double>{1,2,3};
  // output_vector.push_back(4);
  
  // for (int i =0; i < output_vector.size(); i++){
  //   std::cout << output_vector[i] << "\t" ;
  // }
  // std::cout << std::endl;

  // std::string test = "blahblah";

  // if (test.empty()){
  //   std::cout << "String test is empty" << std::endl;
  // }
  // else{
  //   std::cout << "String is not empty. it contains:  " << test << std::endl;
  // }
  // int t_start = 10;
  // int t_end = 20;
  // int t_simulation = 300;
  // ShockDetails shock_test(5,t_start, t_end, t_simulation);

  // std::cout << shock_test.filename << std::endl;

  // std::cout << "Testing enum bit" << std::endl;

  // ShockType shocktype(firing_shock);

  // std::cout << (shock_test.shocktype == shocktype) << std::endl;
  // switch(shock_test.shocktype)
  //   {
  //   case consumption:
  //     std::cout << "Consumption shock" << std::endl;
  //     break;
  //   case debt:
  //     std::cout << "Pure debt shock" << std::endl;
  //     break;
      
  //   case cons_and_debt:
  //     std::cout << "Dual shock" << std::endl;
  //     break;
      
  //   case firing_shock:
  //     std::cout << "Firing shock" << std::endl;
  //     break;
  //   default:
  //     std::cout << "No shock" << std::endl;
  //     break;
  //   }
  
  // json array  = {1,2,3,4,5};
  // json::iterator it  = array.begin();

  // std::cout << *it << std::endl;

  // std::ifstream input_json("../tests/test.json");

  // json json_input;

  // input_json >> json_input;

  // std::cout << "Dumping everything in json" << std::endl;

  // std::cout << json_input <<  std::endl;
  // std::cout << "Iterating over the json object" << std::endl;
  // for (auto& element : json_input){
  //   std::cout << element << std::endl;
  // }

  // std::cout << "Accessing individual entries" << std::endl;

  // for (json::iterator it = json_input.begin(); it != json_input.end(); ++it){
  //   std::cout << it.key() << " :  " << it.value() << std::endl;
  //   }


  // int policy_count = json_input.count("policy");
  // int shock_count = json_input.count("shocks");


  // std::vector<Shocks> all_shocks;

  // std::cout << policy_count << "\t" << shock_count << std::endl;

  // auto& shocks = json_input["shocks"];

  // for (auto& shock: shocks){
  //   std::cout << shock["shock_name"] << std::endl;
  //   int start = shock["shock_start"].get<int>();
  //   int end = shock["shock_end"].get<int>();
  //   Shocks temp_shock(start, end, 2000);
  //   all_shocks.push_back(temp_shock);
    
  // }

  // std::cout << all_shocks[0].t_start << std::endl;
  // //std::cout << typeid(shocks) << std::endl;
  // return 0;
}
