#!/usr/bin/env python3


from stats_utils import *

## We want to run multiple runs with different seeds and check for the crisis_times distributions
##
##
##

import argparse

parser = argparse.ArgumentParser(description = "Run simulations to compute the distributions of T-crisis for various shocks")

parser.add_argument('-shockflag', type=int, default = 1, help = "Flag for the shock")

parser.add_argument('-t_start', type=int , default = 500, help = "When does the shock begin")

parser.add_argument('-nb_runs', type=int, default = 200, help = "Number of independent runs")

# Note that for the rest of the arguments we simply use default values as defined in the run_with_different_seed function
#

args = parser.parse_args()
if __name__ == "__main__":


    shockflag = args.shockflag
    t_start = args.t_start
    nb_runs = args.nb_runs

    fname = flagnames[shockflag]

    fname += f'_{t_start}.csv'

    folder_for_data = os.getcwd() + '/data/'

    if not os.path.exists(folder_for_data):
        os.mkdir(folder_for_data)

    print(os.getcwd())

    crisis_df = run_with_different_seed(shockflag, nb_runs=nb_runs, t_start=t_start, seed_default=-1)

    print("Saving file")

    crisis_df.to_csv(os.path.join(folder_for_data, fname), sep="\t", index = None)
