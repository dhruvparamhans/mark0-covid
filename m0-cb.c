#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sf.h>

#define ZERO            1.e-50

//simulation length
#define	T               10000
#define	Teq             0
#define	tprint          1

//choices
#define RENORM          1           // 1-renormalize to inflation / 0 - no normalization

//demand
#define c0              0.5         // fraction of savings

//revival
#define phi             0.1         // revivial probability per unit of time
#define taupi           0.2

//others
#define delta           0.02        // dividends share

char dividends = 'A';

#define pi_target	0.002
#define e_target	0.98

//GSL stuff
const gsl_rng_type *gslT ;
gsl_rng *gslr ;

double min(double x, double y){ if(x<y) return x; else return y; }
double max(double x, double y){ if(x>y) return x; else return y; }

int main(int argc, char *argv[ ] ){
    
    FILE    *out;
    char    name[100], add[100];
    
    //variables
    double  bust, Pavg, Pold, u, S, Atot, Ap, Am, Ytot, Wtot, e, Wavg, inflation, k, propensity, Dtot, rho, Gamma;
    double  rhom, rhop, rp_avg, rm_avg, pi_avg, u_avg;
    int     N;
    
    //parameters
    double  gammap, gammaw, theta, aGamma, Gamma0, eta0m, eta0p, beta, R, z, rho0, f, alpha_c, alpha_e, alpha_pi;
    
    //others;
    double  Pmin, rp, rw, ren, Pnorm, arg, pay_roll, dY, p, tmp, budget, interests;
    double  Wmax, wage_norm, u_share, deftot;
    int     i, t, seed, new_firm, new_len;
    double  profits;
    
    //array
    double  *P, *Y, *D, *A, *W, *PROFITS;
    int     *ALIVE, *new_list;
    
    if(argc!=16){
        printf("Wrong Input... for example:\n");
        printf("./m0-cb R=2. N=2000 gp=0.05 th=3. aG=50. f=0.5 b=2. z=1. rho0=0.02 file=out seed=0 ac=0.2 ap=0.5 ae=0.5 G0=0.5\n");
        exit(1);
    }
    
    // sscanf(argv[1],"R=%lf",&R);
    //sscanf(argv[2],"N=%d",&N); // Fixed to 10000 in covid
    //sscanf(argv[3],"gp=%lf",&gammap);
    //sscanf(argv[4],"th=%lf",&theta);
    sscanf(argv[5],"aG=%lf",&aGamma);
    //sscanf(argv[6],"f=%lf",&f); //Already set in covid
    //sscanf(argv[7],"b=%lf",&beta); // Fixed in covid
    //sscanf(argv[8],"z=%lf",&z); // Corresponds to r in covid. Already set in covic
    //sscanf(argv[9],"rho0=%lf",&rho0);
    //sscanf(argv[10],"file=%s",add);
    //sscanf(argv[11],"seed=%d",&seed);
    //sscanf(argv[12],"ac=%lf",&alpha_c); // Alpha in covid code 
    //sscanf(argv[13],"ap=%lf",&alpha_pi);
    //sscanf(argv[14],"ae=%lf",&alpha_e);
    //sscanf(argv[15],"G0=%lf",&Gamma0);
    
    eta0m  = 0.1;
    eta0p  = R*eta0m;
    gammaw = z*gammap;
    
    printf("R = %.2e\nN = %d\ntheta = %.2e\ngp = %.2e\tgw = %.2e\nf = %.2e\nb = %.2e\n",R,N,theta,gammap,gammaw,f,beta);
    printf("a_Gamma = %.2e\nalpha_c = %.2e\nGamma0 = %.2e\n",aGamma,alpha_c,Gamma0);
    printf("rho0 = %.2e\nap = %.2e\nae = %.2e\n",rho0,alpha_pi,alpha_e);
    
    //memory
    A = calloc(N,sizeof(double));      	// asset
    Y = calloc(N,sizeof(double));      	// production
    D = calloc(N,sizeof(double));      	// demand
    P = calloc(N,sizeof(double));      	// price
    W = calloc(N,sizeof(double));      	// wage
    PROFITS = calloc(N,sizeof(double)); // firm profits
    ALIVE = calloc(N,sizeof(int));   	// 0 = dead, 1 = active
    
    new_list  = calloc(N,sizeof(int));      	// list of firms to be revived w.p. phi
    
    int     collapse;
    
    /* ************************************** INIT ************************************** */
    
    if(seed==-1)seed = time(NULL);
    
    gsl_rng_env_setup() ;
    gslT = gsl_rng_default ;
    gslr = gsl_rng_alloc(gslT) ;
    gsl_rng_set(gslr,seed);
    
    Pavg = 0.;
    Ytot = 0.;
    Wtot = 0.;
    Atot = 0.;
    Wmax = Wavg = 1.;
    Pold = 1.;
    inflation = pi_avg = 0.;
    rhom = rho = rm_avg = rho0;
    rhop = rp_avg = 0.;
    u_avg = 0.;
    
    for(i=0;i<N;i++){
        ALIVE[i] =  1;
        P[i] 	 =  1.  + 0.1*(2*gsl_rng_uniform(gslr)-1.);
        Y[i] 	 =  0.5 + 0.1*(2*gsl_rng_uniform(gslr)-1.);
        D[i] 	 =  0.5;
        W[i] 	 =  1.;
        PROFITS[i] = P[i]*min(D[i],Y[i]) - W[i]*Y[i];
        
        A[i] =  2*Y[i]*W[i]*gsl_rng_uniform(gslr);
        
        Atot += A[i];
        Ytot += Y[i];
        Pavg += P[i]*Y[i];
        Wtot += Y[i]*W[i];
    }
    
    e = Ytot / N;
    u = 1. - e ;
    
    Pavg /= Ytot;
    Wavg /= Ytot;
    
    S = e*N;
    
    //fix total amount of money to N
    tmp = Atot + S;
    S = S*N/tmp;
    Atot=0.;
    
    for(i=0;i<N;i++){
        A[i]  = A[i]*N/tmp;
        Atot += A[i];
    }
    
    /* *********************************** MAIN CYCLE ************************************ */
    
    sprintf(name,"output/%s.txt",add);
    out = (FILE*)fopen(name,"w");

    collapse = 0;
    
    for(t=0;t<T;t++){
        
        Gamma = max(aGamma * (rm_avg-pi_avg),Gamma0);
        
        //renormalize in unit of price
        if(RENORM==1){
            for(i=0;i<N;i++){
                P[i]/=Pavg;
                W[i]/=Pavg;
                A[i]/=Pavg;
                PROFITS[i] /= Pavg;
            }
            S    /= Pavg;
            Wavg /= Pavg;
            Wmax /= Pavg;
            Pold /= Pavg;
            Pavg  = 1.;
        }
        
        /* *********************************** UPDATE ************************************ */
        
        pi_avg = taupi*inflation + (1.-taupi)*pi_avg;
        rp_avg = taupi*rhop + (1.-taupi)*rp_avg;
        rm_avg = taupi*rhom + (1.-taupi)*rm_avg;
        u_avg  = taupi*u + (1.-taupi)*u_avg;
        
        //update firms variables
        Wtot = 0.;
        Ytot = 0.;
        tmp  = 0.;
        Pmin = 1.e+300 ;
        
        if(beta>0.){
            wage_norm = 0.;
            for(i=0;i<N;i++)if(ALIVE[i]==1){
                arg = beta*(W[i]-Wmax)/Wavg ;
                if(arg>-100.) wage_norm += gsl_sf_exp(arg);
            }
        }
        
        new_len = 0 ;
        deftot = 0.;
        Ap = 0.;
        Am = 0.;
        
        for(i=0;i<N;i++){
            
            // living firms
            if(ALIVE[i]==1){
                
                pay_roll = Y[i]*W[i] ;
                
                // if not bankrupt update price / production / wages and compute interests / savings and debt
                if((A[i]>-theta*pay_roll)||(theta<0.)){
                    
                    if(pay_roll>0.) ren = Gamma * A[i] / pay_roll;
                    else ren = 0.;
                    
                    if (ren> 1.) ren=  1. ;
                    if (ren<-1.) ren= -1. ;
                    
                    rp = gammap*gsl_rng_uniform(gslr);
                    rw = gammaw*gsl_rng_uniform(gslr);
                    
                    dY = D[i] - Y[i] ;
                    p  = P[i];
                    
                    if(beta>0.){
                        arg = beta*(W[i]-Wmax)/Wavg ;
                        u_share = 0.;
                        if(arg>-100.)u_share = u * N * (1.-bust) * gsl_sf_exp(arg) / wage_norm;
                    }
                    
                    else{
                        u_share = u;
                    }
                    
                    //excess demand
                    if(dY>0.){
                        //increase production
                        Y[i] += min(eta0p*(1.+ren)*dY,u_share);
                        //increase price
                        if(p<Pavg)P[i] *= (1. + rp) ;
                        //increase wage
                        if((PROFITS[i]>0.)&&(gammaw>ZERO)){
                            W[i] *= 1. + (1. + ren)*rw*e;
                            W[i] = min(W[i],(P[i]*min(D[i],Y[i])+rhom*min(A[i],0.)+rhop*max(A[i],0.))/Y[i]);
                            W[i] = max(W[i],0.);
                        }
                    }
                    //excess production
                    else {
                        //decrease production
                        Y[i] += eta0m*(1.-ren)*dY ;
                        //decrease price
                        if(p>Pavg)P[i] *= (1. - rp) ;
                        //decrease wage
                        if(PROFITS[i]<0.){
                            W[i] *= 1. - (1.-ren)*rw*u;
                        }
                    }
                    
                    Y[i] = max(Y[i],0.);
                    
                    Wtot += W[i]*Y[i];
                    tmp  += P[i]*Y[i];
                    Ytot += Y[i];
                    
                    Ap   += max(A[i],0.);
                    Am	 -= min(A[i],0.);
                    
                    Pmin = min(Pmin,P[i]);
                    
                    if((P[i]>1.e+30)||(P[i]<1.e-30)){
                        printf("price under/overflow... (1)\n");
                        if(P[i]>1.e+30)collapse=3;
                        if(P[i]<1.e-30)collapse=4;
                        t=T;
                    }
                }
                
                // if bankrupt shut down and compute default costs
                else {
                    deftot -= A[i];
                    Y[i] = 0.;
                    ALIVE[i] = 0;
                    A[i] = 0.;
                    new_list[new_len]=i;
                    new_len++;
                }
            }
            
            else{
                new_list[new_len]=i;
                new_len++;
            }
        }
        
        Pavg = tmp / Ytot ;
        Wavg = Wtot / Ytot ;
        e = Ytot / N ;
        u = 1. - e ;
        
        /* *********************************** INTERESTS ************************************ */
		
        rhom = rho;
        if(Am>0.)rhom += (1.-f)*deftot / Am ;
        
        interests = rhom*Am ;
		
        rhop = k = 0.;
        if( S + Ap > 0. ){
            rhop = (interests - deftot) / ( S + Ap );
            k = Am / ( S + Ap) ;
        }
        
        S += rhop * S ;
        
        /* *********************************** CONSUMPTION ************************************ */
        
        propensity = c0 * (1.+ alpha_c*(pi_avg-rp_avg)/gammap) ;
        
        propensity = max(propensity,0.);
        propensity = min(propensity,1.);
        
        budget = propensity * ( Wtot + max(S,0.) );
        
        Pnorm = 0.;
        for(i=0;i<N;i++)if(ALIVE[i]==1){
            arg = beta*(Pmin-P[i]) / Pavg ;
            if(arg>-100.) Pnorm += gsl_sf_exp(arg);
        }
        
        Dtot = 0.;
        profits = 0.;
        Ap = 0.;
        
        for(i=0;i<N;i++)if(ALIVE[i]==1){
            
            D[i] = 0.;
            
            arg = beta*(Pmin-P[i])/Pavg ;
            
            if(arg>-100.) D[i] = budget * gsl_sf_exp(arg) / Pnorm / P[i];
            
            PROFITS[i]  = P[i]*min(Y[i],D[i]) - Y[i]*W[i] + rhom*min(A[i],0.) + rhop*max(A[i],0.);
            
            S          -= P[i]*min(Y[i],D[i]) - Y[i]*W[i];
            A[i]       += PROFITS[i];
            
            if((A[i]>0.)&&(PROFITS[i]>0.)){
                if(dividends=='P'){
                    S    += delta*PROFITS[i];
                    A[i] -= delta*PROFITS[i];
                }
                if(dividends=='A'){
                    S    += delta*A[i];
                    A[i] -= delta*A[i];
                }
            }
            
            Dtot    += D[i];
            profits += PROFITS[i];
            
            Ap += max(A[i],0.);
            
        }
        
        /* ******************************* REVIVAL ******************************** */
        
        //revival
        deftot = 0.;
        
        for(i=0;i<new_len;i++)if(gsl_rng_uniform(gslr)<phi){
            
            new_firm    = new_list[i];
            Y[new_firm] = max(u,0.)*gsl_rng_uniform(gslr);
            ALIVE[new_firm] = 1;
            P[new_firm] = Pavg;
            W[new_firm] = Wavg;
            
            A[new_firm] = W[new_firm]*Y[new_firm];
            deftot  += A[new_firm];
            
            Ap += A[new_firm];
            
            PROFITS[new_firm] = 0.;
            
        }
        
        /* ******************************* FINAL ******************************** */
        
        //new averages
        tmp  = 0.;
        Ytot = 0.;
        Wtot = 0.;
        bust = 0.;
        Wmax = 0.;
        Atot = 0.;
        Am = 0.;
        
        for(i=0;i<N;i++){
            
            //final averages
            if(ALIVE[i]==1){
                
                if((Ap>0.)&&(A[i]>0.))A[i] -= deftot*A[i]/Ap;
                
                Wtot += Y[i]*W[i];
                Ytot += Y[i];
                tmp  += P[i]*Y[i];
                
                Wmax = max(W[i],Wmax);
                
                Am 	 -= min(A[i],0.);
                Atot += A[i];
                
                
            }
            
            else bust += 1./N;
            
        }
        
        Pavg = tmp / Ytot;
        Wavg = Wtot / Ytot;
        
        inflation = (Pavg-Pold)/Pold;
        Pold = Pavg;
        
        e = Ytot / N  ;
        
        if((e-1>1.e-10)||(e<-ZERO)||(S<0.)){
            printf("Error!! -> e = %.10e\tS=%.10e\n",e,S);
            collapse=2;
            t=T;
        }
        
        e = min(e,1.);
        e = max(e,0.);
        
        u =  1. - e;
        
        if(Ytot<1.e-07){
            printf("Collapse\n");
            collapse = 1;
            t=T;
        }
        
        /************************************** OUTPUT ************************************/
        
        if(t%tprint==0){
            fprintf(out,"%d\t%e\t%e\t%e\t%e\t",t+1,u,bust,Pavg,Wavg);
            fprintf(out,"%e\t%e\t%e\t%e\t",S,Atot,Ap,Am);
            fprintf(out,"%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n",inflation,pi_avg,propensity,k,Dtot,rhom,rhop,rho);
            fflush(out);
        }
        
        /************************************** CB ************************************/
        
        tmp = min((1.-u_avg)*1.025,e_target);
        
        rho = rho0 + alpha_pi * 10 * ( pi_avg - pi_target ) + alpha_e * gsl_sf_log((1.-u_avg)/tmp);
        rho = max(0.,rho);
        
    }
    
    fclose(out);
    
    free(ALIVE);
    free(A);
    free(P);
    free(D);
    free(Y);
    free(W);
    free(PROFITS);
    free(new_list);
    
    printf("Done\n");
    
}
