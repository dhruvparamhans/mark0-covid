#include<iostream>
#include<fstream>
#include<sstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sf.h>
#include <vector>
#include <string>
#include <map>
#include <algorithm> 
#include "utils.hpp"
#include <random>
#include <numeric>
using namespace std;

/*! \def ZERO
    \brief Sets lower bound for checking for zero.
*/

 
/*! \def DOANTICIPATE
    \brief Flag to set if agents make expectations about future inflation
*/


 
/*! \Def FACPI
    \brief TODO
*/

 
/*! \def T
    \brief Total Simulation time 
*/

 
/*! \def Teq
    \brief Number of time steps to equilibrate the system. 
*/

 
/*! \def tprint
    \brief Time period after which to print results
*/

 
/*! \def RENORM
    \brief Flag to renormalize prices to inflation or not. 
*/

 
/*! \def G0
    \brief Fraction of savings. Dunno where this enters. 
*/

 
/*! \def phi
    \brief Firm revival rate.
*/
 
/*! \def taupi
    \brief Parameter for exponential moving average. \f$\omega\f$ in the paper. 
*/

/*! \def delta
    \brief Fractions of dividends given to households. 
*/

/*! \fn double min (double x, double y)
    \brief Computes the minimum of two numbers.
    \param x First number 
    \param y Second number 
*/


/*! \fn double max (double x, double y)
    \brief Computes the maximum of two numbers.
    \param x First number 
    \param y Second number 
*/

#define ZERO            1.e-10
#define DOANTICIPATE    true
#define FACPI           0

char dividends = 'A';

//simulation length
#define     Tsim            7000
#define     Teq             200
#define     tprint          1

//choices
//#define RENORM          1           // 1-renormalize to inflation / 0 - no normalization

//demand
#define G0              0.5         // fraction of savings

//revival
#define phi             0.1         // revivial probability per unit of time
#define taupi           0.2    

//others
#define delta           0.02        // dividends share



//GSL stuff
const gsl_rng_type *gslT ;
gsl_rng *gslr ;

double min(double x, double y){ if(x<y) return x; else return y; }
double max(double x, double y){ if(x>y) return x; else return y; }
double red(double x){ return ( fabs(x) < ZERO ? 0.0 : x);}

double compute_ema(double previous, double current,  double omega=0.2){
  return omega*current + (1-omega)*previous;
}

void update_variable_shock(double & var, double factor){
  var /= factor; 
}

double bernouilli_trial(std::mt19937 &gen){
  std::bernoulli_distribution distrib(0.5);

  if (distrib(gen)){
      return 1.0;
  }
  else{
    return -1.0;
  }
}



template<typename T, typename A>
T sum_vector(std::vector<T,A> const& data ){
    T sum = 0.0;

    for (typename std::vector<T,A>::const_iterator it = data.begin(); it != data.end(); ++it){
        sum += *it;
    }
    return sum; 
}

template<typename T, typename A>
T variance_vector(std::vector<T,A> const& data){
    T mean = sum_vector(data)/data.size();
    T square = std::inner_product(data.begin(), data.end(), data.begin(), 0.0);

    return square/data.size() - mean*mean;
}

double variance_vector2(Vector const & data){
    //double mean = sum_vector(data)/data.size();
    double mean = 0.0;

    double var = 0.0;
    for (int i= 0; i < data.size(); i++){

        var += data[i]*data[i];
        mean += data[i];
    }

    mean /= data.size();

    return var/data.size() - (mean*mean);
}



void mark0loop(int argc, char *argv[ ]){
    
    FILE    *out;
    char    add[10000], name[10000];
 
    double G = G0;
    
    //variables
    double  bust, Pavg, Pold, u, S, Atot, firm_savings, debt_tot, Ytot, Wtot, e, Wavg, inflation, k, propensity, Dtot, rho;
    double  rhom, rhop, rp_avg, pi_avg, Gamma, u_avg, rm_avg;
    int     N;
    
    //parameters
    double  gammap, gammaw, theta, alpha_g, eta0m, eta0p, beta, R, r, alpha, rho0, f, alpha_e, alpha_pi, Gamma0;
    
    //others;
    double  Pmin, rp, rw, ren, Pnorm, arg, pay_roll, dY, p , tmp, budget, interests;
    double  Wmax, wage_norm, u_share, deftot;
    int     i, t, seed, new_firm, new_len;
    double  profits;
    
    double pi_target, e_target; //target inflation and employment for CB
    int    negative_count=0;
    double tau_meas , tau_tar;
    double wage_factor;
    
     
    f = 0.5; // is this c_0
    beta = 2.0; // Price sensitivity parameter
    r = 1.0; // what is this ?
//    N = 10000;  // Number of firms
    
    double y0 = 0.5;
    double etam_new; // New value for firing rate
                     //
    double factor = 0.0; // Factor by which consumption goes down during consumption shock.

    double zeta = 0.0; // Productivity factor 
    double ptol;
    double zfactor;
    double relax;
    int helico;
    int gpolicy;
    int adapt;
    // Preparing the program

    //Parse the arguments passed to program

    Parameters program_params("covid", "Running covid shocks on Mark0");
    
    if (argc <2){
      //std::cout << program_params.options->help() << std::endl;
      std::cout << "Running with default options" << std::endl;
    }

    VectorPtr ptr_vec = {&R, &theta, &Gamma0, &rho0, &alpha, &alpha_pi, &alpha_e, &pi_target, &e_target, &tau_tar, &wage_factor,&y0, &gammap, &eta0m, &tau_meas, &alpha_g, &etam_new, &factor, &zeta, &zfactor, &ptol, &relax};


   
   
    int shockflag, t_start, t_end, policy_start, policy_end, extra_start, extra_end;
    std::vector<int * > ptr_vec_int = { &seed, &shockflag, &t_start, &t_end, &policy_start, &policy_end, &extra_start, &extra_end,&helico , &N, &gpolicy, &adapt} ;
    
    program_params.parse_cmdline(argc, argv, ptr_vec, ptr_vec_int);
    String path_name = "output/";
    ShockDetails shock_details;
    try{
        shock_details = ShockDetails(shockflag, t_start, t_end, Tsim);

    } catch (const std::invalid_argument& e){
        std::cerr << "exception: " << e.what() << " Flag supplied was " << shockflag << std::endl;
        std::cout << "Accepted flags from 0-6" << std::endl;
        return ;
    }
    //ShockDetails shock_details(shockflag, t_start, t_end, Tsim);
    String s_add;
    if (program_params.outputname.empty()){
        s_add = shock_details.filename;
        std::cout << "Filename " << s_add << std::endl;
    }
    else{
      s_add = program_params.outputname;
    }
    
   
    String final_file_name = path_name + s_add;


    // sprintf(name,"%s.txt",final_file_name.c_str()); // Output to file
    // out = (FILE*)fopen(name,"w");

    FileWriter output (final_file_name + ".txt");
    Vector output_vector;
    Shocks extra_shock(extra_start, extra_end, Tsim);
    String underscore = std::string("_");



    String firm_output_p = "firms_prices"; //Store prices
    String firm_output_w = "firms_wages"; // Store wages
                                               //
    FileWriter price_output(path_name + s_add + underscore + firm_output_p + ".txt");
    FileWriter wage_output(path_name + s_add + underscore + firm_output_w + ".txt");

    
    // if (shockflag != 3 &&  shockflag != 6){
    //     std::cout << "Am I entering here" << std::endl;
    //     std::cout << "Shock flag here " << shockflag << std::endl;
    //     std::cout << "Total simulation time " << T << std::endl;
    //     extra_shock.no_shock();
    // }

    Shocks debt_policy(policy_start, policy_end, Tsim);
    if (shockflag <= 3 || shockflag > 6 ){
        debt_policy.no_shock();
    }
    //array
    Vector  P(N), Y(N), D(N), A(N), W(N), PROFITS(N);
    std::vector<int>     ALIVE(N), new_list(N);
    Vector etaplus(N), etaminus(N); // to store hiring and firing rates
    Vector wage_nominal(N);
    
    double R0 = R;
    eta0p  = R*eta0m;
    gammaw = r*gammap;

    int t_helico = t_start + 500; 

    
    printf("R = %.2e\nN = %d\ntheta = %.2e\ngp = %.2e\tgw = %.2e\nf = %.2e\nb = %.2e\nalphag = %.2e\nalpha=%f\n\n",R,N,theta,gammap,gammaw,f,beta,alpha_g,alpha);
    printf("rho0 = %.2e\tap = %.2e\tae = %.2e\n",rho0,alpha_pi,alpha_e);
    printf("pit = %.2e\tet = %.2e\n",pi_target,e_target);
    printf("taut = %.2e\ttaum = %.2e\n",tau_tar,tau_meas);
    printf("eta0m= %.2e\teta0p = %.2e\n",eta0m,eta0p);
    printf("seed %d\n",seed);
    
    char params[10000];
    sprintf(params,"%.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e",rho0,alpha_pi,alpha_e,pi_target,e_target,theta,R,alpha_g,Gamma0,alpha,tau_meas,tau_tar);
    
    //final averages
    double  avg_u, avg_bu, avg_k, avg_pi, avg_rho, avg_rhom, avg_rhop;
    double  sig_u, sig_bu, sig_k, sig_pi;
    double  max_u, min_u, max_pi, min_pi, max_rho, min_rho, max_rhom, min_rhom, max_rhop, min_rhop;
    
    int     collapse, avg_counter;
    
    double theta0 = theta;
    double zeta0 = zeta; 
    /* ************************************** INIT ************************************** */
    
    if(seed==-1)
        seed = time( NULL );
    
    // gsl_rng_env_setup() ;
    // gslT = gsl_rng_default ;
    // gslr = gsl_rng_alloc(gslT) ;
    // gsl_rng_set(gslr,seed);
    std::cout << "Using STL Generator" << std::endl; 
    std::mt19937 gen(seed);
    std::uniform_real_distribution<double> dis(0.0, 1.0);

    std::bernoulli_distribution dis_bern(0.5);
    
    
    Pavg = 0.;
    Ytot = 0.;
    Wtot = 0.;
    Atot = 0.;
    Wmax = Wavg = 1.;
    Pold = 1.;
    inflation = pi_avg = 0.;
    rhom = rho = rm_avg = rho0;
    rhop = rp_avg = 0.;
    u_avg = 0.;
     /* *********************************** INIT  ************************************ */
    for(i=0;i<N;i++){
        
        ALIVE[i] =  1; //firms
        P[i] 	 =  1.  + 0.1*(2*dis(gen)-1.); //firm
        Y[i] 	 =  y0  + 0.1*(2*dis(gen)-1.); //firm
	//P[i] 	 =  1.  + 0.1*(2*gsl_rng_uniform(gslr)-1.);
        //Y[i] 	 =  y0  + 0.1*(2*gsl_rng_uniform(gslr)-1.);
        Y[i]     *= zeta; 
        D[i] 	 =  y0; //firm
        W[i] 	 =  zeta; //firm
        PROFITS[i] = P[i]*min(D[i],Y[i]) - W[i]*Y[i]; //firm
	wage_nominal[i] = W[i];
        
        A[i] =  2.0*Y[i]*W[i]*dis(gen); // cash balance //firm
        //A[i] =  2.0*Y[i]*W[i]*gsl_rng_uniform(gslr);
        Atot += A[i];
        Ytot += Y[i];
        Pavg += P[i]*Y[i];
        Wtot += Y[i]*W[i];

	//etaplus[i] = 0.0;
	//etaminus[i] = 0.0;
	
        
    }


    e = Ytot / N; //eco
    e /= zeta; 
    u = 1. - e ;
    
    Pavg /= Ytot;
    Wavg = Wtot/Ytot;
    
    S = e*N; //hh
    //S = 0.0;
    double a0 = 0.0;
    //fix total amount of money to N
    double M0 = a0*N;
    tmp = Atot + S;
    S = S*(N*a0)/tmp;
    Atot=0.;
    
    for(i=0;i<N;i++){
        A[i]  = A[i]*(N*a0)/tmp;
        Atot += A[i];
    }

    std::cout << "S = " << S << " Atot = " << Atot << std::endl;

    std::cout << "zeta " << zeta << " zfactor " << zfactor << " relax " << relax << std::endl;


    double w_nominal = Wavg;
    double product_p = 1.0;
    
     /* *********************************** END INIT ************************************ */
    
    /* *********************************** MAIN CYCLE ************************************ */
    avg_counter = collapse = 0;
    avg_bu = avg_k = avg_pi = avg_u = 0.;
    avg_rho = avg_rhom = avg_rhop = 0.;
    sig_bu = sig_k = sig_pi = sig_u = 0.;
    
    max_rho = max_rhom = max_rhop = -1.0;
    min_rho = min_rhom = min_rhop = +1.0;
    max_pi = -1.0;
    min_pi = +1.0;
    
    
    max_u = 0.;
    min_u = 2.;
    
    bust=0.;
    
    if( alpha_pi < ZERO )
    {
        pi_target = 0.0;
        tau_tar   = 0.0;
    }

    bool renorm = true;
    int true_end = 0;
    
 
    
    for(t=0;t<Tsim;t++){

      // //Stop renormalization during the shock
      // if (shock_details.shock.shock_array[t] | extra_shock.shock_array[t]){
      // 	std::cout << "Switching off Renorm" << std::endl;
      // 	renorm=false;
      // }
      // else{
      // 	renorm = true;
      // }
      
        //renormalize in unit of price        
        if(renorm){
            for(i=0;i<N;i++){
                P[i]/=Pavg;
                W[i]/=Pavg;
                A[i]/=Pavg;
                PROFITS[i] /= Pavg;
            }
            
            S    /= Pavg;
            Wavg /= Pavg;
            Wmax /= Pavg;
            Pold /= Pavg;
            M0 /= Pavg; 
            Pavg  = 1.;
            
        }
        
        /* *********************************** UPDATE ************************************ */
        
        pi_avg = taupi*inflation + (1.-taupi)*pi_avg;
        rp_avg = taupi*rhop + (1.-taupi)*rp_avg;
        rm_avg = taupi*rhom + (1.-taupi)*rm_avg;
        u_avg = taupi*u + (1.-taupi)*u_avg;
        
        //update firms variables
        Wtot = 0.;
        Ytot = 0.;
        tmp  = 0.;
        Pmin = 1.e+300 ;



	// Boltzmann average for updating unemployment share
        if(beta>0.){
            wage_norm = 0.;
            for(i=0;i<N;i++)if(ALIVE[i]==1){
                arg = beta*(W[i]-Wmax)/Wavg ;
                if(arg > -100.) wage_norm += std::exp(arg);
            }
        }       
        new_len = 0 ;
        deftot = 0.;
        firm_savings = 0.;
        debt_tot = 0.;
        
        double pi_used = tau_tar * pi_target + tau_meas * pi_avg ;
        double frag_avg = 0.0; // Average fragility
        Gamma = max(alpha_g * (rm_avg-pi_used),Gamma0); //set Gamma 
        for(i=0;i<N;i++){
            
            // living firms
            if(ALIVE[i]==1){
                pay_roll = (Y[i]*W[i]/zeta) ; //Denominator for most quantities here
                
                // if not bankrupt update price / production / wages and compute interests / savings and debt
                 //frag_avg += (-1.*A[i]/pay_roll);
                if((A[i] > -theta*pay_roll)||(theta<0.)){

                    frag_avg += (-1.*A[i]*Y[i]/pay_roll);
                    if(pay_roll>0.){
                        ren = Gamma * A[i] / pay_roll;
                    }  //$\Gamma \Epsilon_{i}/(W_{i} Y_{i})
                    else{
                        ren = 0.;
                    }
                    
                    // this is not in the current code
                    if (ren> 1.) ren=  1. ;
                    if (ren < -1.) ren= -1. ;

                    rp = gammap*dis(gen); // $\gamma_{p} \xi_{i}(t)$
                    rw = gammaw*dis(gen); // $\gamma_{w} \xi_{t}(t)$
                    //rp = gammap*gsl_rng_uniform(gslr);
		    //rw = gammaw*gsl_rng_uniform(gslr);
                    dY = D[i] - Y[i] ;
                    p  = P[i];
                    
                    if(beta>0.){
                        arg = beta*(W[i]-Wmax)/Wavg ;
                        u_share = 0.;
                        if(arg > -100.)u_share = u * N * (1.-bust) * std::exp(arg) / wage_norm; //Eqn(A9)
                    }
                    
                    else{
                        u_share = u;
                    }
                    
                    //excess demand
                    if(dY>0.){

                        //increase production
                        double eta = eta0p*(1.+ren); //Eqn(10) from Paper2
                        if(eta<0.0){
                            eta=0.0;
                        }// Clipping values between [0,1]
                        if(eta>1.0){
                            eta=1.0;
                        }
                        etaplus[i] = eta; // store this firms eta_{+}
                        Y[i] += std::min(eta*dY,u_share*zeta); // Eqn (9) -> Excess demand paper2
                        
                        //increase price
                        if(p<Pavg){
                            P[i] *= (1. + rp) ; // Eqn(A8) -> top bit. from paper 1
                        }
                        //increase wage
                        if((PROFITS[i]>0.)&&(gammaw>ZERO)){
                            // why do you have gamma_w > 0 condition
                            W[i] *= 1. + (1.0+ren) * rw * e; //Eqn(A11) top bit. Sign change because we compute the fragility factor without the negative sign.
                            //W[i] = min(W[i],(P[i]*min(D[i],Y[i])+rhom*min(A[i],0.)+rhop*max(A[i],0.))/Y[i]); // Set wages so that profits never go to zero even with this new wage.
                            W[i] = std::min(W[i], zeta*(P[i]*std::min(D[i], Y[i]) + rhom*std::min(A[i],0.) + rhop*std::max(A[i],0.0))/(Y[i]));
                            W[i] = std::max(W[i],0.); // Wages can never be zero.
                        }
                    }
                    
                    //excess production
                    else {
                        
                        //decrease production
                        double eta = eta0m*(1.-ren);// Eqn(10) paper2
                        if(eta<0.0){
                            eta=0.0;// Clipping values between [0,1]
                        }
                        if(eta>1.0){
                            eta=1.0;
                        }
                        etaminus[i] = eta; // Store this firms eta_{-}
                        Y[i] += eta*dY ; // Eqn 9 Paper2 
                        
                        //decrease price
                        if(p>Pavg){
                            P[i] *= (1. - rp) ; //Eqn 12 (lower bit)
                        }
                        //decrease wage
                        if(PROFITS[i]<0.){
                            W[i] *= 1. - (1.0-ren)*rw*u; // Eqn (A11) lower bit
                            W[i] = std::max(W[i],0.);
                        }
                    }
                    
                    if(DOANTICIPATE){
                        P[i] *= 1.0 + pi_used; //set inflation expectations
                        W[i] *= 1.0 + wage_factor * pi_used; // set inflation expectations
                    }
                    
                    Y[i] = std::max(Y[i],0.);
                    
                    Wtot += W[i]*Y[i];
                    tmp  += P[i]*Y[i];
                    Ytot += Y[i];
                    
                    firm_savings += std::max(A[i],0.); // Eqn (A7) paper1 
                    debt_tot     -= std::min(A[i],0.); // Eqn (A7) paper 1
                    
                    Pmin = std::min(Pmin,P[i]);
                    
                    if((P[i]>1.0/ZERO)||(P[i]<ZERO)){
                        printf("price under/overflow... (1)\n");
                        if(P[i]>1.0/ZERO)collapse=3;
                               if (P[i]<ZERO) collapse=4;
                        t=Tsim;
                    }
                }
                
                // if bankrupt shut down and compute default costs
                else { // This company was alive in the previous time-step but has gone bankrupt now 
                    deftot -= A[i]; // Eqn(A6) Paper1
                    Y[i] = 0.;
                    ALIVE[i] = 0;
                    A[i] = 0.;
                    new_list[new_len]=i;
                    new_len++;
                }
            }
            // for companies already dead
            else{ 
                new_list[new_len]=i;
                new_len++;
            }
        }
        
        Pavg = tmp / Ytot ; // Average price pbar = p_{i} Y_{i} /(\sum_{i} Y_{i})
        Wavg = Wtot / Ytot ; // Average wage = W_{i} Y_{i}/(sum_{i} Y_{i})

        //frag_avg /= Ytot;

        e = Ytot / N ;//Computing employment
        e /= zeta;

        u = 1. - e ; // Unemployment

        double ytot_temp = Ytot; 
        /* *********************************** INTERESTS ************************************ */
        // rhom = rho;
        // rhop = 0;
        // if there are default costs
        //
        double left = 0.0;
        if (fabs(S + firm_savings - deftot - debt_tot - M0) > 0.0){

            left = S + firm_savings - deftot - debt_tot - M0;
            if ( fabs(left) > (S+firm_savings)*ZERO ){
                std::cout << "Huge problem" << std::endl;
                std::cout << t << "\t" << S+firm_savings << "\t" << deftot <<  "\t" << debt_tot << "\t" << S + firm_savings - deftot - debt_tot << "\t" << left <<  std::endl ;
                 //std::cout << t << "\t" <<  rhom << "\t" << rhop << "\t" << rhom*debt_tot << "\t" << rhop*(S+firm_savings) << "\t" << deftot << "\t" << rhom*debt_tot - rhop*(S+firm_savings) - deftot << std::endl;

                 exit(1);

            }

            else{
                S -= left;
            }

            if (S < 0.0){
                std::cout << "Negative Savings" << std::endl;
                exit(1);
            }
        }


        double temp_rhom;
        double temp_rhop; 

        temp_rhom = rho;
        if(debt_tot>0.0)temp_rhom += (1.-f)*deftot / debt_tot ; // Change f -> 1-f in Eqn(7) Paper1
        
        interests = temp_rhom*debt_tot ;
        
        temp_rhop = k = 0.;
        if( S + firm_savings > 0. ){
            temp_rhop = (interests - deftot) / ( S + firm_savings ); // From Eqn(8) Paper1
            k = debt_tot / ( S + firm_savings) ; // what is k here
        }


        if (temp_rhop <= -0.5){
             std::cout << t << " Deposit Rate too low rhop = " << temp_rhop << " Debt-tot "  << debt_tot << " Deftot " << deftot << " S " << S << std::endl;
             std::cout << "Old rhom " << temp_rhom << " Old rhop " << temp_rhop << std::endl;
        }
        rhop = temp_rhop;
        rhom = temp_rhom;
        S += rhop*S;
        if (temp_rhop <= -0.5){
            std::cout << t  << " Savings now " << S << std::endl; 
        }
        //std::cout << t << " Savings during crisis " << S << " rhop " << rhop <<   std::endl;
        /* *********************************** CONSUMPTION ************************************ */
        /* ******************************* SHOCK HAPPENS HERE ********************************* */
        
        propensity = G * (1.+ alpha*(pi_used-rp_avg) ) ; // Eqn(5) paper 2 
        propensity = std::max(propensity,0.); // Clipping values between [0,1]
        propensity = std::min(propensity,1.); // This one too.
       
        // Todo
        // Need to separate out the shocks a bit more
        if(shock_details.shock.shock_array[t])
        {
            std::cout << "Consumption shock" << std::endl;
            //update_variable_shock(propensity,2);
            propensity = propensity*factor;
            std::cout << t << " propensity " << propensity << " " << G << " " << alpha << std::endl;

            switch(shock_details.shocktype)
            {
                case firing:
                    std::cout << "Firing shock and consumption shock" << std::endl;
                    eta0m = 0.4;
                    std::cout << t << "Hiring rate " << eta0p << " Firing rate " << eta0m << " propensity " << propensity << std::endl;
                    break;
                case production:
                    std::cout << "Production shock" << std::endl;
                    zeta = zfactor*zeta0;
                    for (int i =0; i< Y.size(); i++){
                        Y[i] *= zfactor;
                    }
                    Ytot *= zfactor;
                    std::cout << t << " Current zeta " << zeta << " Old zeta " << zeta0 << std::endl; 
                    break;
                // case fire_and_hire:
                //     std::cout << "Firing shock and consumption shock" << std::endl;
                //     eta0m = 0.4; //Double the firing rate
                //      std::cout << t << "Hiring rate " << eta0p << " Firing rate " << eta0m << " propensity " << propensity << std::endl;
                //     break;
                case fire_and_debt:
                    eta0m = 0.4;
                    std::cout << t << "Hiring rate " << eta0p << " Firing rate " << eta0m << " propensity " << propensity << std::endl;
                    break;
                case prod_debt:
                    std::cout << "Production shock" << std::endl;
                    zeta = zfactor * zeta0;
		    for (int i = 0; i< Y.size(); i++){
		     Y[i] *= zfactor;
		     }
		    Ytot *= zfactor;
		    
                    std::cout << t << " Current zeta " << zeta <<  " Old Zeta " << zeta0 << std::endl;
                    break;

                // case prod_debt:
                //     eta0m = 0.4;
                //     std::cout << t << "Hiring rate " << eta0p << " Firing rate " << eta0m << " propensity " << propensity << std::endl;
                //     break;
                    
                default:
                    break;
            }
        }
        else{
            eta0m = 0.2;
            eta0p = 0.4;
            for (int i = 0; i < Y.size(); i++){
                Y[i] *= zeta0/zeta;
            }
            Ytot *= zeta0/zeta;
            zeta = zeta0;
        }

        int adapt_length = static_cast<int>(relax);
        if (t>= t_end && t <= t_end+adapt_length && adapt == 1){
            std::cout << "Increasing consumption" << std::endl;
            propensity = 0.5+0.2;
            //propensity = 0.5 + ((0.5 - factor*0.5)/(relax))*(t - (t_end +adapt_length));
            std::cout << t << " propensity " << propensity << " " << G << " " << alpha << std::endl;
        }






        if (debt_policy.shock_array[t]){
            true_end = t;
            if (gpolicy == 1){
                //std::cout << "Smart policy being applied " << std::endl;
                if (t <= policy_end){
                    theta = theta0*100;
                    std::cout << t << " Theta =  " << theta << std::endl;
                    debt_policy.shock_array[t+1] = true;
                }
                else if (ptol*(frag_avg/Ytot) > theta0){
                    theta = max(theta0, ptol*frag_avg/Ytot);
                    std::cout << t << "Theta = " << theta << "frag-avg = " << frag_avg << std::endl;
                    debt_policy.shock_array[t+1] = true;
                }
                else{
                    theta = theta0;
                    std::cout << t << "Theta = " << theta << std::endl;
                }
            }
            else{
                std::cout << "Naive policy being applied" << std::endl;
                theta = theta0*100;
                std::cout << t << "Theta = " << theta << std::endl;
            }
        }

        else{
            theta = theta0;
            Gamma0 = 0.0;
        }


    if (t ==t_end){
        if (helico ==1 & shockflag > 0){
            std::cout << t << " Perform Helicopter drop" << std::endl;
            std::cout << "Increasing Savings of Households" << std::endl;
            double S0 = S;
            S += 0.5*S0;
            M0 +=  0.5*S0;
        }
    }
    
        
    budget = propensity * ( Wtot/zeta + std::max(S,0.) ); // Eqn (5) Paper 2
        
        Pnorm = 0.; // Boltzmann average for price. Used in eqn (A3) paper 1 
        for(i=0;i<N;i++)if(ALIVE[i]==1){
            arg = beta*(Pmin-P[i]) / Pavg ;
            if(arg > -100.) Pnorm += std::exp(arg);
        }
        
        Dtot = 0.;
        profits = 0.;
        firm_savings = 0.;
        
        for(i=0;i<N;i++)if(ALIVE[i]==1){
            
            D[i] = 0.;
            
            arg = beta*(Pmin-P[i])/Pavg ;
            
            if(arg > -100.) D[i] = budget * std::exp(arg) / Pnorm / P[i]; //Eqn (A3) Paper 1

            PROFITS[i]  = P[i]*std::min(Y[i],D[i]) - (Y[i]*W[i]/zeta) + rhom*std::min(A[i],0.) + rhop*std::max(A[i], 0.);

            // if (rhop <= -0.9){
            //     PROFITS[i] += (rhop*max(A[i],0.));
            // }
            // else{
            //     PROFITS[i] += rhop*max(A[i], 0.);
            // }

            // Eqn (A12) Paper 1 
            S          -= P[i]*std::min(Y[i],D[i]) - (Y[i]*W[i]/zeta); // Eqn (A14) without dividend term
            A[i]       += PROFITS[i]; // Eqn (A13) without dividend term


	    //Dividend payments
            if((A[i]>0.)&&(PROFITS[i]>0.)){
	      //Dividends paid as a fraction of the profits
                if(dividends=='P'){
                    S    += delta*PROFITS[i];
                    A[i] -= delta*PROFITS[i];
                }
		//Dividends paid as a fraction of the cash balance
                if(dividends=='A'){
                    S    += delta*A[i];
                    A[i] -= delta*A[i];
                }
            }
            
            Dtot    += D[i];
            profits += PROFITS[i];
            
            firm_savings += max(A[i],0.);
            
            }
        // Beyond this point there is only the revival bit and nothing else
        // So we define the u-rate and bust here
        //
        double u_bef = u;
        double bust_bef = (N - sum_vector(ALIVE))/N;
        if (temp_rhop <= -0.5){
	  std::cout << t << " Deposit Rate too low budget = " << budget << " Dtot "  << Dtot << " firm_savings " << firm_savings << " S " << S << " Pnorm " << Pnorm << " Wtot " << Wtot << " Ptot " << tmp << " Ytot " << Ytot << " u " << u << " bust " << bust_bef << " Pavg " << Pavg <<  " Diff " << tmp - Ytot << std::endl;
        }
        
        /* ******************************* REVIVAL ******************************** */
        
        //revival
        deftot = 0.;

	// Change this to dis(gen) < phi
	//gsl_rng_uniform(gslr)<phi
        for(i=0;i<new_len;i++)if(dis(gen)<phi){
            
            new_firm = new_list[i];
            Y[new_firm] = std::max(u,0.)*dis(gen);
	    //changes for reinit - Stan
	    //D[new_firm] = Y[new_firm]; 
	    //D[new_firm] = Y[new_firm] + (1e-6*(2.0*dis(gen)-1.0));
	    //	    D[new_firm] = Y[new_firm] + (1e-16*dis(gen)); 
	    
	    //D[new_firm] = dis(gen); 
	    //D[new_firm] = y0; 
	    //D[new_firm] = Y[new_firm]*(1+ 1e-6*bernouilli_trial(gen));
	    //D[new_firm] = Y[new_firm]; 
            ALIVE[new_firm] = 1;
            if (u_bef == 1 ){
	      std::cout << "Unemployment is 100%. Restarting economy " << std::endl;
		
	      //P[new_firm] = 1.0 + (1e-6 * bernouilli_trial(gen));
	      P[new_firm] = 1.0 + (1e-6 * (2.0*dis(gen)-1.0)); 
		W[new_firm] = 1.0; 
                A[new_firm] = 0.0;
		//D[new_firm] = Y[new_firm] * (1e-6 * (2.0*dis(gen)-1.0));
		//D[new_firm] = Y[new_firm] * (1 + 1e-6 * bernouilli_trial(gen));
            }
            else{
	      P[new_firm] = Pavg;
	      //P[new_firm] = Pavg + (1e-6 * (2.0*dis(gen)-1.0)); 
                W[new_firm] = Wavg;
                A[new_firm] = (W[new_firm]*Y[new_firm]); 
            }
            // Adding revival term
           // D[new_firm] = 1.1*Y[new_firm];
            deftot  += A[new_firm];
            
            firm_savings += A[new_firm];
            
            PROFITS[new_firm] = 0.;
            }


        // for (int i = 0; i < N; i++){
        //     D[i] = Y[i]*1.1;
        // }
        
        /* ******************************* FINAL ******************************** */
        
        //new averages
        tmp  = 0.;
        Ytot = 0.;
        Wtot = 0.;
        bust = 0.;
        Wmax = 0.;
        Atot = 0.;
        double etaplus_avg = 0.0;
        double etaminus_avg = 0.0;
        debt_tot = 0.;
        int firms_alive = 0;

        
        for(i=0;i<N;i++){
            
            //final averages
            if(ALIVE[i]==1){
                
                if((firm_savings>0.)&&(A[i]>0.))A[i] -= deftot*A[i]/firm_savings; // Why is this step necessary?
                
                Wtot    += Y[i]*W[i];
                Ytot    += Y[i];
                tmp     += P[i]*Y[i];
                
                Wmax = std::max(W[i],Wmax);
                
                debt_tot -= std::min(A[i],0.);
                Atot     += A[i];

                etaplus_avg += etaplus[i];
                etaminus_avg += etaminus[i];
                firms_alive +=1;
                
            }
            
            else bust += 1./N;
            
        }
        
        Pavg = tmp / Ytot;
        Wavg = Wtot / Ytot;
        
        inflation = (Pavg-Pold)/Pold;
        Pold = Pavg;
        
        e = Ytot / N  ;

        double p_var = variance_vector(P);
        double w_var = variance_vector(W);

        
        if((e-1 > ZERO)|| (e < -ZERO) || (S < 0.0) ){
            printf("Error!! -> t =%d\t e = %.10e\tS=%.10e\n",t+1,e,S);
            std::cout << "Zeta here " << zeta << " Propensity " << propensity << " Atot " << Atot << " firm savings " << firm_savings << std::endl;
            collapse=2;
            t=Tsim;
        }
        e /= zeta;
        e = std::min(e,1.);
        e = std::max(e,0.);
        
        u =  1. - e;

        if(Ytot<ZERO){
            printf("Collapse\n");
            collapse = 1;
            t=Tsim;
        }

         if (temp_rhop <= -0.5){
            std::cout << t << " Deposit Rate too low budget = " << budget << " Atot "  << Atot << " firm_savings " << firm_savings << " S " << S << " Pnorm " << Pnorm << " Wtot " << Wtot << " Ptot " << tmp << " Ytot " << Ytot << " u " << u << " bust " << bust_bef << " Pavg " << Pavg <<  " debt_tot " << debt_tot << " deftot " << deftot << std::endl;
         }




         // We store variables at t = start-10, start-5, [start, start+20]


         // if (t == t_start-10 || t == t_start-5 ||   (t >= t_start &&  t <= t_start+30) || t == t_start+50 || t == t_start + 100){

         //     std::cout << "Writing to file at t = " << t << "\t" << variance_vector(P) << "\t" << variance_vector2(P) << std::endl;
         //     price_output.write_vector_to_file(P);
         //     wage_output.write_vector_to_file(W);
         // }


	// if (extra_shock.shock_array[t] | shock_details.shock.shock_array[t] | t == 2990 | t == 3050){
	//   // product_p *= Pavg;
	//   // w_nominal = Wavg*product_p;
	//   String firmoutput_filename = "firms" + underscore + std::to_string(t);
	  
	//   FileWriter firm_output (path_name + firmoutput_filename + ".txt");

	//   std::cout << "Saving file " << firmoutput_filename << std::endl;
	//   firm_output.write_vector_to_file(A);
	//   firm_output.write_vector_to_file(W);
	//   firm_output.write_vector_to_file(P);
	//   firm_output.write_vector_to_file(Y);
	//   firm_output.write_vector_to_file(D);

	//   firm_output.close_file();
	// }
	// else{
	//   w_nominal = Wavg;
	// }
	
	
        
        /************************************** CB ************************************/
        
        
        tmp = min((1.-u_avg)*1.025,e_target);
        
        rho = rho0 + FACPI * pi_target + alpha_pi * ( pi_avg - pi_target ) ;//+ alpha_e * gsl_sf_log((1.-u_avg)/tmp);
        
        /************************************** OUTPUT ************************************/
        
        
        if((t%tprint==0)){
            double R_temp = eta0p/eta0m;
            // Implementing the output allows for easily adding other variables to
            // be saved into output vector.
            output_vector = Vector{static_cast<double>(t), u_bef, bust_bef, Pavg, Wavg, S, Atot, firm_savings, debt_tot, inflation, pi_avg, propensity, k, Dtot, rhom, rho, rhop, pi_used, tau_tar, tau_meas, R_temp};

            output_vector.push_back(Wtot);
            output_vector.push_back(etaplus_avg/firms_alive);
            output_vector.push_back(etaminus_avg/firms_alive);
            output_vector.push_back(w_nominal);
            output_vector.push_back(Ytot);
            output_vector.push_back(deftot);
            output_vector.push_back(profits);
            output_vector.push_back(debt_tot/S);
            output_vector.push_back(static_cast<double>(firms_alive));
            output_vector.push_back(left);
            output_vector.push_back(u);
            output_vector.push_back(bust);
            output_vector.push_back(frag_avg/Ytot);
            output_vector.push_back(true_end);
            output_vector.push_back(theta);
            output_vector.push_back(p_var);
            output_vector.push_back(w_var);
	    output_vector.push_back(ytot_temp);
	    output_vector.push_back(std::min(Ytot, ytot_temp));


            output.write_vector_to_file(output_vector);

            output_vector.clear();
        }



        if(t>=Teq){
            
            if( rho < -ZERO )
                negative_count++;
            
            avg_u  += u;
            avg_pi += inflation;
            avg_rho += rho;
            avg_bu += bust;
            avg_k  += k;
            avg_rhom += rhom;
            avg_rhop += rhop;
            
            sig_u  += u*u;
            sig_pi += inflation*inflation;
            sig_bu += bust*bust;
            sig_k  += k*k;
            
            max_u = max(u,max_u);
            min_u = min(u,min_u);
            max_pi = max(inflation,max_pi);
            min_pi = min(inflation,min_pi);
            max_rho = max(rho,max_rho);
            min_rho = min(rho,min_rho);
            max_rhom =  max(rhom,max_rhom);
            min_rhom =  min(rhom,min_rhom);
            max_rhop =  max(rhop,max_rhop);
            min_rhop =  min(rhop,min_rhop);
            
            avg_counter++;
            
        }
        
    }
    
    if(avg_counter==0){
        max_u = 1.;
        min_u = 0.;
        avg_u = 1.;
        avg_pi = 0.;
        avg_bu = 1.;
        avg_k = 0.;
        sig_u = sig_bu = sig_k = sig_pi = 0.;
    }
    
    else{
        avg_k  /= avg_counter;
        avg_pi /= avg_counter;
        avg_rho /= avg_counter;
        avg_bu /= avg_counter;
        avg_u  /= avg_counter;
        avg_rhom /= avg_counter;
        avg_rhop  /= avg_counter;
        
        sig_k  /= avg_counter;
        sig_pi /= avg_counter;
        sig_bu /= avg_counter;
        sig_u  /= avg_counter;
        
        sig_k  -= avg_k*avg_k   ;
        sig_bu -= avg_bu*avg_bu ;
        sig_pi -= avg_pi*avg_pi ;
        sig_u  -= avg_u*avg_u   ;
        
    }
    
    // fclose(out);
    output.close_file();

    
    double p_neg = double(negative_count)/avg_counter;
    
    char vars[10000];
    sprintf(vars,"\nu: %.2e %.2e %.2e\npi: %.2e %.2e %.2e\noth: %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e",red(avg_u),red(max_u),red(min_u),red(avg_pi),red(max_pi),red(min_pi),
            red(avg_rho),red(max_rho),red(min_rho),red(avg_rhom),red(max_rhom),red(min_rhom),red(avg_rhop),red(max_rhop),red(min_rhop),red(p_neg));
    
    printf("\n RESULTS:\n%s %s\n",params,vars);
    
}
