h_cols =    ['t',
             'u',
             'bust',
             'Pavg',
             'Wavg',
             'S',
             'Atot',
             'firm-savings',
             'debt-tot',
             'inflation',
             'pi-avg',
             'propensity',
             'k',
             'Dtot',
             'rhom',
             'rho',
             'rhop',
             'pi-used',
             'tau-tar',
             'tau-meas',
             'R', 
            'Wtot',
            'etap-avg',
            'etam-avg', 
            'w-nominal',
            'Ytot',
             'deftot', 
            'profits',
            'debt-ratio',
            'firms-alive',
             'left']


import os
import glob
import numpy as np
import pandas as pd

flagnames = ['cons_pure', 'firing_shock', 'fire_hire', 'cons_theta', 'fire_debt', 'fire_hire_debt']
flag_dict = { x+1: flagnames[x] for x in range(6)}

paths = [flagname + '/' for flagname in flagnames]


def find_intersection(data, threshold):
    #taken from here: https://stackoverflow.com/a/23868444
    
    thresholded_data = data <= threshold
    threshold_edges = np.convolve([1, -1], thresholded_data, mode='same')

    crisis_ends = np.where(threshold_edges == 1)[0]
    crisis_starts = np.where(threshold_edges == -1)[0]
    return crisis_starts,crisis_ends


def get_all_files(shockflag):
    flagname = flag_dict[shockflag]
    path = flagname + '/'
    return [os.path.basename(x) for x in glob.glob(path + flagname + '*')]


def read_file(fname):
    tmp = pd.read_csv(fname, sep="\t", header = None)
    tmp.columns = h_cols
    return tmp

def get_t_start(fname):
    return float(fname.split('_')[-2])

def get_index(fname):
    return float(fname.split('_')[-1].split('.')[0])


def get_length(fname):
    return float(fname.split('_')[-3])
def get_factor(fname):
    return float(fname.split('_')[-2])



def get_error_rate_limited(shock_length, factor, shockflag=1, t_start=1):


    flagname = flag_dict[shockflag]

    all_files = get_all_files(shockflag)

    filter_length = list(filter(lambda x: get_length(x) == shock_length, all_files))

    filter_factor = list(filter(lambda x: get_factor(x) == factor, filter_length))

    error =[]
    for fname in filter_factor:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)

        if crisis_ends.size < 1:
            error.append(1)
        else:
            error.append(0)

    return np.mean(error)

def get_crisis_times_fixed_length(shock_length, shockflag=1, t_start=1000):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)

    filter_files = list(filter(lambda x: get_length(x) == shock_length), all_files)

    index = []
    factors = []
    crisis_times = [] # only a single crisis time


    error = []

    for fname in filter_files:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)

        seed = get_index(fname)
        factor = get_factor(fname)

        index.append(seed)
        factor.append(factor)

        if (crisis_ends.size >1):
            crisis_times.append(crisis_ends[1])
            error.append(0)
        else:
            crisis_times.append(len(res['u'][t_start+1:]))
            error.append(1)
    return pd.DataFrame({
        'seed': index,
        'crisis': crisis_times,
        'factor': factors,
        'length' : length
    })



def generate_stats(shockflag, t_start):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)
    filter_files = list(filter(lambda x: get_t_start(x) == t_start, all_files))
    index = []
    crisis_times = []
    crisis_times_2 = []
    between_crisis_times = []
    total_crisis_times = []
    error = []
    
    for fname in filter_files:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)

        seed = get_index(fname)
        index.append(seed)

        if (shockflag >=4):
            if (crisis_ends.size > 1):
                crisis_times_2.append(crisis_ends[-1] - crisis_start[-1])
                between_crisis_times.append(crisis_start[-1] - crisis_ends[1])
                total_crisis_times.append(crisis_ends[-1])
        if (crisis_ends.size >1):
            crisis_times.append(crisis_ends[1])
            error.append(0)
            #print('seed = {}, t-final = {}'.format(seed, crisis_ends[1]))
        else:
            crisis_times.append(len(res['u'][t_start+1:]))
            error.append(1)
            #print('seed = {}, t-final = error'.format(seed))
    if (shockflag >=4):
        return pd.DataFrame({
            'seed': index, 
            'first-crisis': crisis_times,
            'second-crisis': crisis_times_2,
            'between' : between_crisis_times, 
            'total' : total_crisis_times,
            'is_error': error})
    
#         return crisis_times, crisis_times_2, between_crisis_times, total_crisis_times
    else:
        return pd.DataFrame({
            'seed': index,
            'crisis': crisis_times,
            'error': error})



def compute_stats_and_save(shockflag):
    basename = 'stats'
    t_starts = [500, 1000, 1500, 2000, 3000]

    for t_start in t_starts:
        print("Running for T-Start = {}".format(t_start))
        stats_df = generate_stats(shockflag, t_start)
        fname = basename + '_{}_{}.csv'.format(flag_dict[shockflag], t_start)
        stats_df.to_csv(fname, sep="\t", index = None)


# for shockflag in range(1,7):
#     print("Running for Shock = {}".format(flag_dict[shockflag]))
#     compute_stats_and_save(shockflag)


def get_complete_error_rates(shockflag=1, t_start=1000):
    flagname = flag_dict[shockflag]
    all_files = get_all_files(shockflag)

    index = []
    factors= []
    lengths = []
    crisis_times = []
    error = []
    is_crisis = []
    for fname in all_files:
        res = read_file(os.path.join(flagname, fname))
        crisis_start, crisis_ends = find_intersection(res['u'][t_start+1:], 0.1)
        lengths.append(get_length(fname))
        index.append(get_index(fname))
        factors.append(get_factor(fname))

        if (crisis_ends.size > 1):
            crisis_times.append(crisis_ends[1] - crisis_start[0])
            error.append(0)
            is_crisis.append(1)
            print("Index = {}, Length = {}, Factor = {}, T-crisis = {}".format(get_index(fname), get_length(fname), get_factor(fname), crisis_ends[1]))
        else:
            if (crisis_start.size < 1):
                ## Then no crisis
                is_crisis.append(0)
                crisis_times.append(0)
                error.append(1)
                print("Index = {}, Length = {}, Factor = {}, No Crisis".format(get_index(fname), get_length(fname), get_factor(fname)))
            else:
                crisis_times.append(len(res['u'][t_start+1:]))
                is_crisis.append(1)
                error.append(1)
                print("Index = {}, Length = {}, Factor = {}, T-crisis = error".format(get_index(fname), get_length(fname), get_factor(fname)))

    return pd.DataFrame({
        'index': index,
        'factor': factors,
        'length' : lengths,
        'error' : error,
        'is-crisis': is_crisis, 
        'T-crisis': crisis_times})



# lengths=[4, 5, 6, 7, 8, 9, 10]
# factors=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99]


# error_rates = []

# for length in lengths:
#     for factor in factors:
#         error_rate = get_error_rate_limited(length, factor)
#         error_rates.append(error_rate)
#         print("Done for Crisis-length = {}, Factor = {}, proba = {}".format(length, factor, error_rate))


# complete_df = pd.DataFrame({
#     'length' : lengths,
#     'factor' : factors,
#     'error' : error_rates
# })


# print("Saving to file")

complete_df = get_complete_error_rates()
print("Saving to file")
fname = 'long_crisis_rate'+ '.csv'
complete_df.to_csv(fname, sep="\t", index = None)

